package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.bodyfitness.modelo.Cliente;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * Cliente entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.Cliente
 */
@Scope("singleton")
@Repository("ClienteDAO")
public class ClienteDAO extends JpaDaoImpl<Cliente, Long> implements IClienteDAO {
    private static final Logger log = LoggerFactory.getLogger(ClienteDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IClienteDAO getFromApplicationContext(ApplicationContext ctx) {
        return (IClienteDAO) ctx.getBean("ClienteDAO");
    }
    
	@Override
	public Cliente BuscarClientePorCedula(Long cedula) {
		Cliente cliente = new Cliente();
		try {
			cliente = (Cliente)entityManager.createQuery("FROM Cliente cli WHERE cli.cedula=:cedula").setParameter("cedula", cedula).getSingleResult();
		} catch (Exception e) {
			cliente =  null;
		}
		
		return cliente;
	}
}
