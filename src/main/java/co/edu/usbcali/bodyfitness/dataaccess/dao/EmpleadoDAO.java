package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.bodyfitness.modelo.Empleado;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * Empleado entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.Empleado
 */
@Scope("singleton")
@Repository("EmpleadoDAO")
public class EmpleadoDAO extends JpaDaoImpl<Empleado, Long>
    implements IEmpleadoDAO {
    private static final Logger log = LoggerFactory.getLogger(EmpleadoDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IEmpleadoDAO getFromApplicationContext(ApplicationContext ctx) {
        return (IEmpleadoDAO) ctx.getBean("EmpleadoDAO");
    }
}
