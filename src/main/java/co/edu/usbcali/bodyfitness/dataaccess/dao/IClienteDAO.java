package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.Cliente;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   ClienteDAO.
*
*/
public interface IClienteDAO extends Dao<Cliente, Long> {
	public Cliente BuscarClientePorCedula(Long cedula);
}
