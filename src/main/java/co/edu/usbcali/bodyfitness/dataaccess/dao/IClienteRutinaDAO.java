package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   ClienteRutinaDAO.
*
*/
public interface IClienteRutinaDAO extends Dao<ClienteRutina, Long> {
}
