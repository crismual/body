package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   EjercicioRutinaDAO.
*
*/
public interface IEjercicioRutinaDAO extends Dao<EjercicioRutina, Long> {
}
