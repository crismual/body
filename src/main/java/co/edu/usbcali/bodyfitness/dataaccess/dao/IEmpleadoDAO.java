package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.Empleado;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   EmpleadoDAO.
*
*/
public interface IEmpleadoDAO extends Dao<Empleado, Long> {
}
