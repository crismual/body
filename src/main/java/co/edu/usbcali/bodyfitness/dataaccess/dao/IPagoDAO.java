package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.Pago;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   PagoDAO.
*
*/
public interface IPagoDAO extends Dao<Pago, Long> {
}
