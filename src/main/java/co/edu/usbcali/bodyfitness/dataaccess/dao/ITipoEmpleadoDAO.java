package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   TipoEmpleadoDAO.
*
*/
public interface ITipoEmpleadoDAO extends Dao<TipoEmpleado, Long> {
}
