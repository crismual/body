package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.Dao;
import co.edu.usbcali.bodyfitness.modelo.TipoPago;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   TipoPagoDAO.
*
*/
public interface ITipoPagoDAO extends Dao<TipoPago, Long> {
}
