package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.bodyfitness.modelo.Rutina;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * Rutina entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.Rutina
 */
@Scope("singleton")
@Repository("RutinaDAO")
public class RutinaDAO extends JpaDaoImpl<Rutina, Long> implements IRutinaDAO {
    private static final Logger log = LoggerFactory.getLogger(RutinaDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IRutinaDAO getFromApplicationContext(ApplicationContext ctx) {
        return (IRutinaDAO) ctx.getBean("RutinaDAO");
    }
}
