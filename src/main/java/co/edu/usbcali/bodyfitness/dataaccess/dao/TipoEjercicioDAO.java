package co.edu.usbcali.bodyfitness.dataaccess.dao;

import co.edu.usbcali.bodyfitness.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * TipoEjercicio entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.TipoEjercicio
 */
@Scope("singleton")
@Repository("TipoEjercicioDAO")
public class TipoEjercicioDAO extends JpaDaoImpl<TipoEjercicio, Long>
    implements ITipoEjercicioDAO {
    private static final Logger log = LoggerFactory.getLogger(TipoEjercicioDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static ITipoEjercicioDAO getFromApplicationContext(
        ApplicationContext ctx) {
        return (ITipoEjercicioDAO) ctx.getBean("TipoEjercicioDAO");
    }
}
