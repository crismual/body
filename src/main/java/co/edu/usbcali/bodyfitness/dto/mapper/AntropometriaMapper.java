package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Antropometria;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.AntropometriaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class AntropometriaMapper implements IAntropometriaMapper {
    private static final Logger log = LoggerFactory.getLogger(AntropometriaMapper.class);

    @Transactional(readOnly = true)
    public AntropometriaDTO antropometriaToAntropometriaDTO(
        Antropometria antropometria) throws Exception {
        try {
            AntropometriaDTO antropometriaDTO = new AntropometriaDTO();

            antropometriaDTO.setAntId(antropometria.getAntId());
            antropometriaDTO.setAbdomen((antropometria.getAbdomen() != null)
                ? antropometria.getAbdomen() : null);
            antropometriaDTO.setActivo((antropometria.getActivo() != null)
                ? antropometria.getActivo() : null);
            antropometriaDTO.setAntebrazo((antropometria.getAntebrazo() != null)
                ? antropometria.getAntebrazo() : null);
            antropometriaDTO.setBrazoContraido((antropometria.getBrazoContraido() != null)
                ? antropometria.getBrazoContraido() : null);
            antropometriaDTO.setBrazoRelajado((antropometria.getBrazoRelajado() != null)
                ? antropometria.getBrazoRelajado() : null);
            antropometriaDTO.setCintura((antropometria.getCintura() != null)
                ? antropometria.getCintura() : null);
            antropometriaDTO.setCuello((antropometria.getCuello() != null)
                ? antropometria.getCuello() : null);
            antropometriaDTO.setEstatura((antropometria.getEstatura() != null)
                ? antropometria.getEstatura() : null);
            antropometriaDTO.setFechaCreacion(antropometria.getFechaCreacion());
            antropometriaDTO.setFechaModificacion(antropometria.getFechaModificacion());
            antropometriaDTO.setHombro((antropometria.getHombro() != null)
                ? antropometria.getHombro() : null);
            antropometriaDTO.setPantorrilla((antropometria.getPantorrilla() != null)
                ? antropometria.getPantorrilla() : null);
            antropometriaDTO.setPecho((antropometria.getPecho() != null)
                ? antropometria.getPecho() : null);
            antropometriaDTO.setPeso((antropometria.getPeso() != null)
                ? antropometria.getPeso() : null);
            antropometriaDTO.setPierna((antropometria.getPierna() != null)
                ? antropometria.getPierna() : null);
            antropometriaDTO.setUsuarioCreador((antropometria.getUsuarioCreador() != null)
                ? antropometria.getUsuarioCreador() : null);
            antropometriaDTO.setUsuarioModificador((antropometria.getUsuarioModificador() != null)
                ? antropometria.getUsuarioModificador() : null);

            return antropometriaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Antropometria antropometriaDTOToAntropometria(
        AntropometriaDTO antropometriaDTO) throws Exception {
        try {
            Antropometria antropometria = new Antropometria();

            antropometria.setAntId(antropometriaDTO.getAntId());
            antropometria.setAbdomen((antropometriaDTO.getAbdomen() != null)
                ? antropometriaDTO.getAbdomen() : null);
            antropometria.setActivo((antropometriaDTO.getActivo() != null)
                ? antropometriaDTO.getActivo() : null);
            antropometria.setAntebrazo((antropometriaDTO.getAntebrazo() != null)
                ? antropometriaDTO.getAntebrazo() : null);
            antropometria.setBrazoContraido((antropometriaDTO.getBrazoContraido() != null)
                ? antropometriaDTO.getBrazoContraido() : null);
            antropometria.setBrazoRelajado((antropometriaDTO.getBrazoRelajado() != null)
                ? antropometriaDTO.getBrazoRelajado() : null);
            antropometria.setCintura((antropometriaDTO.getCintura() != null)
                ? antropometriaDTO.getCintura() : null);
            antropometria.setCuello((antropometriaDTO.getCuello() != null)
                ? antropometriaDTO.getCuello() : null);
            antropometria.setEstatura((antropometriaDTO.getEstatura() != null)
                ? antropometriaDTO.getEstatura() : null);
            antropometria.setFechaCreacion(antropometriaDTO.getFechaCreacion());
            antropometria.setFechaModificacion(antropometriaDTO.getFechaModificacion());
            antropometria.setHombro((antropometriaDTO.getHombro() != null)
                ? antropometriaDTO.getHombro() : null);
            antropometria.setPantorrilla((antropometriaDTO.getPantorrilla() != null)
                ? antropometriaDTO.getPantorrilla() : null);
            antropometria.setPecho((antropometriaDTO.getPecho() != null)
                ? antropometriaDTO.getPecho() : null);
            antropometria.setPeso((antropometriaDTO.getPeso() != null)
                ? antropometriaDTO.getPeso() : null);
            antropometria.setPierna((antropometriaDTO.getPierna() != null)
                ? antropometriaDTO.getPierna() : null);
            antropometria.setUsuarioCreador((antropometriaDTO.getUsuarioCreador() != null)
                ? antropometriaDTO.getUsuarioCreador() : null);
            antropometria.setUsuarioModificador((antropometriaDTO.getUsuarioModificador() != null)
                ? antropometriaDTO.getUsuarioModificador() : null);

            return antropometria;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<AntropometriaDTO> listAntropometriaToListAntropometriaDTO(
        List<Antropometria> listAntropometria) throws Exception {
        try {
            List<AntropometriaDTO> antropometriaDTOs = new ArrayList<AntropometriaDTO>();

            for (Antropometria antropometria : listAntropometria) {
                AntropometriaDTO antropometriaDTO = antropometriaToAntropometriaDTO(antropometria);

                antropometriaDTOs.add(antropometriaDTO);
            }

            return antropometriaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Antropometria> listAntropometriaDTOToListAntropometria(
        List<AntropometriaDTO> listAntropometriaDTO) throws Exception {
        try {
            List<Antropometria> listAntropometria = new ArrayList<Antropometria>();

            for (AntropometriaDTO antropometriaDTO : listAntropometriaDTO) {
                Antropometria antropometria = antropometriaDTOToAntropometria(antropometriaDTO);

                listAntropometria.add(antropometria);
            }

            return listAntropometria;
        } catch (Exception e) {
            throw e;
        }
    }
}
