package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class ClienteAntropometriaMapper implements IClienteAntropometriaMapper {
    private static final Logger log = LoggerFactory.getLogger(ClienteAntropometriaMapper.class);

    /**
    * Logic injected by Spring that manages Antropometria entities
    *
    */
    @Autowired
    IAntropometriaLogic logicAntropometria1;

    /**
    * Logic injected by Spring that manages Cliente entities
    *
    */
    @Autowired
    IClienteLogic logicCliente2;

    @Transactional(readOnly = true)
    public ClienteAntropometriaDTO clienteAntropometriaToClienteAntropometriaDTO(
        ClienteAntropometria clienteAntropometria) throws Exception {
        try {
            ClienteAntropometriaDTO clienteAntropometriaDTO = new ClienteAntropometriaDTO();

            clienteAntropometriaDTO.setCliantId(clienteAntropometria.getCliantId());
            clienteAntropometriaDTO.setActivo((clienteAntropometria.getActivo() != null)
                ? clienteAntropometria.getActivo() : null);
            clienteAntropometriaDTO.setFechaCreacion(clienteAntropometria.getFechaCreacion());
            clienteAntropometriaDTO.setFechaModificacion(clienteAntropometria.getFechaModificacion());
            clienteAntropometriaDTO.setUsuarioCreador((clienteAntropometria.getUsuarioCreador() != null)
                ? clienteAntropometria.getUsuarioCreador() : null);
            clienteAntropometriaDTO.setUsuarioModificador((clienteAntropometria.getUsuarioModificador() != null)
                ? clienteAntropometria.getUsuarioModificador() : null);

            if (clienteAntropometria.getAntropometria() != null) {
                clienteAntropometriaDTO.setAntId_Antropometria(clienteAntropometria.getAntropometria()
                                                                                   .getAntId());
            } else {
                clienteAntropometriaDTO.setAntId_Antropometria(null);
            }

            if (clienteAntropometria.getCliente() != null) {
                clienteAntropometriaDTO.setCliId_Cliente(clienteAntropometria.getCliente()
                                                                             .getCliId());
            } else {
                clienteAntropometriaDTO.setCliId_Cliente(null);
            }

            return clienteAntropometriaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public ClienteAntropometria clienteAntropometriaDTOToClienteAntropometria(
        ClienteAntropometriaDTO clienteAntropometriaDTO)
        throws Exception {
        try {
            ClienteAntropometria clienteAntropometria = new ClienteAntropometria();

            clienteAntropometria.setCliantId(clienteAntropometriaDTO.getCliantId());
            clienteAntropometria.setActivo((clienteAntropometriaDTO.getActivo() != null)
                ? clienteAntropometriaDTO.getActivo() : null);
            clienteAntropometria.setFechaCreacion(clienteAntropometriaDTO.getFechaCreacion());
            clienteAntropometria.setFechaModificacion(clienteAntropometriaDTO.getFechaModificacion());
            clienteAntropometria.setUsuarioCreador((clienteAntropometriaDTO.getUsuarioCreador() != null)
                ? clienteAntropometriaDTO.getUsuarioCreador() : null);
            clienteAntropometria.setUsuarioModificador((clienteAntropometriaDTO.getUsuarioModificador() != null)
                ? clienteAntropometriaDTO.getUsuarioModificador() : null);

            Antropometria antropometria = new Antropometria();

            if (clienteAntropometriaDTO.getAntId_Antropometria() != null) {
                antropometria = logicAntropometria1.getAntropometria(clienteAntropometriaDTO.getAntId_Antropometria());
            }

            if (antropometria != null) {
                clienteAntropometria.setAntropometria(antropometria);
            }

            Cliente cliente = new Cliente();

            if (clienteAntropometriaDTO.getCliId_Cliente() != null) {
                cliente = logicCliente2.getCliente(clienteAntropometriaDTO.getCliId_Cliente());
            }

            if (cliente != null) {
                clienteAntropometria.setCliente(cliente);
            }

            return clienteAntropometria;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ClienteAntropometriaDTO> listClienteAntropometriaToListClienteAntropometriaDTO(
        List<ClienteAntropometria> listClienteAntropometria)
        throws Exception {
        try {
            List<ClienteAntropometriaDTO> clienteAntropometriaDTOs = new ArrayList<ClienteAntropometriaDTO>();

            for (ClienteAntropometria clienteAntropometria : listClienteAntropometria) {
                ClienteAntropometriaDTO clienteAntropometriaDTO = clienteAntropometriaToClienteAntropometriaDTO(clienteAntropometria);

                clienteAntropometriaDTOs.add(clienteAntropometriaDTO);
            }

            return clienteAntropometriaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ClienteAntropometria> listClienteAntropometriaDTOToListClienteAntropometria(
        List<ClienteAntropometriaDTO> listClienteAntropometriaDTO)
        throws Exception {
        try {
            List<ClienteAntropometria> listClienteAntropometria = new ArrayList<ClienteAntropometria>();

            for (ClienteAntropometriaDTO clienteAntropometriaDTO : listClienteAntropometriaDTO) {
                ClienteAntropometria clienteAntropometria = clienteAntropometriaDTOToClienteAntropometria(clienteAntropometriaDTO);

                listClienteAntropometria.add(clienteAntropometria);
            }

            return listClienteAntropometria;
        } catch (Exception e) {
            throw e;
        }
    }
}
