package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Cliente;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class ClienteMapper implements IClienteMapper {
    private static final Logger log = LoggerFactory.getLogger(ClienteMapper.class);

    @Transactional(readOnly = true)
    public ClienteDTO clienteToClienteDTO(Cliente cliente)
        throws Exception {
        try {
            ClienteDTO clienteDTO = new ClienteDTO();

            clienteDTO.setCliId(cliente.getCliId());
            clienteDTO.setActivo((cliente.getActivo() != null)
                ? cliente.getActivo() : null);
            clienteDTO.setApellido((cliente.getApellido() != null)
                ? cliente.getApellido() : null);
            clienteDTO.setCedula((cliente.getCedula() != null)
                ? cliente.getCedula() : null);
            clienteDTO.setCelular((cliente.getCelular() != null)
                ? cliente.getCelular() : null);
            clienteDTO.setCorreo((cliente.getCorreo() != null)
                ? cliente.getCorreo() : null);
            clienteDTO.setDireccion((cliente.getDireccion() != null)
                ? cliente.getDireccion() : null);
            clienteDTO.setFechaCreacion(cliente.getFechaCreacion());
            clienteDTO.setFechaModificacion(cliente.getFechaModificacion());
            clienteDTO.setFechaNacimiento(cliente.getFechaNacimiento());
            clienteDTO.setFoto((cliente.getFoto() != null) ? cliente.getFoto()
                                                           : null);
            clienteDTO.setNombre((cliente.getNombre() != null)
                ? cliente.getNombre() : null);
            clienteDTO.setPassword((cliente.getPassword() != null)
                ? cliente.getPassword() : null);
            clienteDTO.setSexo((cliente.getSexo() != null) ? cliente.getSexo()
                                                           : null);
            clienteDTO.setUsuarioCreador((cliente.getUsuarioCreador() != null)
                ? cliente.getUsuarioCreador() : null);
            clienteDTO.setUsuarioModificador((cliente.getUsuarioModificador() != null)
                ? cliente.getUsuarioModificador() : null);

            return clienteDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Cliente clienteDTOToCliente(ClienteDTO clienteDTO)
        throws Exception {
        try {
            Cliente cliente = new Cliente();

            cliente.setCliId(clienteDTO.getCliId());
            cliente.setActivo((clienteDTO.getActivo() != null)
                ? clienteDTO.getActivo() : null);
            cliente.setApellido((clienteDTO.getApellido() != null)
                ? clienteDTO.getApellido() : null);
            cliente.setCedula((clienteDTO.getCedula() != null)
                ? clienteDTO.getCedula() : null);
            cliente.setCelular((clienteDTO.getCelular() != null)
                ? clienteDTO.getCelular() : null);
            cliente.setCorreo((clienteDTO.getCorreo() != null)
                ? clienteDTO.getCorreo() : null);
            cliente.setDireccion((clienteDTO.getDireccion() != null)
                ? clienteDTO.getDireccion() : null);
            cliente.setFechaCreacion(clienteDTO.getFechaCreacion());
            cliente.setFechaModificacion(clienteDTO.getFechaModificacion());
            cliente.setFechaNacimiento(clienteDTO.getFechaNacimiento());
            cliente.setFoto((clienteDTO.getFoto() != null)
                ? clienteDTO.getFoto() : null);
            cliente.setNombre((clienteDTO.getNombre() != null)
                ? clienteDTO.getNombre() : null);
            cliente.setPassword((clienteDTO.getPassword() != null)
                ? clienteDTO.getPassword() : null);
            cliente.setSexo((clienteDTO.getSexo() != null)
                ? clienteDTO.getSexo() : null);
            cliente.setUsuarioCreador((clienteDTO.getUsuarioCreador() != null)
                ? clienteDTO.getUsuarioCreador() : null);
            cliente.setUsuarioModificador((clienteDTO.getUsuarioModificador() != null)
                ? clienteDTO.getUsuarioModificador() : null);

            return cliente;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ClienteDTO> listClienteToListClienteDTO(
        List<Cliente> listCliente) throws Exception {
        try {
            List<ClienteDTO> clienteDTOs = new ArrayList<ClienteDTO>();

            for (Cliente cliente : listCliente) {
                ClienteDTO clienteDTO = clienteToClienteDTO(cliente);

                clienteDTOs.add(clienteDTO);
            }

            return clienteDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Cliente> listClienteDTOToListCliente(
        List<ClienteDTO> listClienteDTO) throws Exception {
        try {
            List<Cliente> listCliente = new ArrayList<Cliente>();

            for (ClienteDTO clienteDTO : listClienteDTO) {
                Cliente cliente = clienteDTOToCliente(clienteDTO);

                listCliente.add(cliente);
            }

            return listCliente;
        } catch (Exception e) {
            throw e;
        }
    }
}
