package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class ClienteRutinaMapper implements IClienteRutinaMapper {
    private static final Logger log = LoggerFactory.getLogger(ClienteRutinaMapper.class);

    /**
    * Logic injected by Spring that manages Cliente entities
    *
    */
    @Autowired
    IClienteLogic logicCliente1;

    /**
    * Logic injected by Spring that manages Rutina entities
    *
    */
    @Autowired
    IRutinaLogic logicRutina2;

    @Transactional(readOnly = true)
    public ClienteRutinaDTO clienteRutinaToClienteRutinaDTO(
        ClienteRutina clienteRutina) throws Exception {
        try {
            ClienteRutinaDTO clienteRutinaDTO = new ClienteRutinaDTO();

            clienteRutinaDTO.setClirutId(clienteRutina.getClirutId());
            clienteRutinaDTO.setActivo((clienteRutina.getActivo() != null)
                ? clienteRutina.getActivo() : null);
            clienteRutinaDTO.setFechaCreacion(clienteRutina.getFechaCreacion());
            clienteRutinaDTO.setFechaModificacion(clienteRutina.getFechaModificacion());
            clienteRutinaDTO.setUsuarioCreador((clienteRutina.getUsuarioCreador() != null)
                ? clienteRutina.getUsuarioCreador() : null);
            clienteRutinaDTO.setUsuarioModificador((clienteRutina.getUsuarioModificador() != null)
                ? clienteRutina.getUsuarioModificador() : null);

            if (clienteRutina.getCliente() != null) {
                clienteRutinaDTO.setCliId_Cliente(clienteRutina.getCliente()
                                                               .getCliId());
            } else {
                clienteRutinaDTO.setCliId_Cliente(null);
            }

            if (clienteRutina.getRutina() != null) {
                clienteRutinaDTO.setRutId_Rutina(clienteRutina.getRutina()
                                                              .getRutId());
            } else {
                clienteRutinaDTO.setRutId_Rutina(null);
            }

            return clienteRutinaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public ClienteRutina clienteRutinaDTOToClienteRutina(
        ClienteRutinaDTO clienteRutinaDTO) throws Exception {
        try {
            ClienteRutina clienteRutina = new ClienteRutina();

            clienteRutina.setClirutId(clienteRutinaDTO.getClirutId());
            clienteRutina.setActivo((clienteRutinaDTO.getActivo() != null)
                ? clienteRutinaDTO.getActivo() : null);
            clienteRutina.setFechaCreacion(clienteRutinaDTO.getFechaCreacion());
            clienteRutina.setFechaModificacion(clienteRutinaDTO.getFechaModificacion());
            clienteRutina.setUsuarioCreador((clienteRutinaDTO.getUsuarioCreador() != null)
                ? clienteRutinaDTO.getUsuarioCreador() : null);
            clienteRutina.setUsuarioModificador((clienteRutinaDTO.getUsuarioModificador() != null)
                ? clienteRutinaDTO.getUsuarioModificador() : null);

            Cliente cliente = new Cliente();

            if (clienteRutinaDTO.getCliId_Cliente() != null) {
                cliente = logicCliente1.getCliente(clienteRutinaDTO.getCliId_Cliente());
            }

            if (cliente != null) {
                clienteRutina.setCliente(cliente);
            }

            Rutina rutina = new Rutina();

            if (clienteRutinaDTO.getRutId_Rutina() != null) {
                rutina = logicRutina2.getRutina(clienteRutinaDTO.getRutId_Rutina());
            }

            if (rutina != null) {
                clienteRutina.setRutina(rutina);
            }

            return clienteRutina;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ClienteRutinaDTO> listClienteRutinaToListClienteRutinaDTO(
        List<ClienteRutina> listClienteRutina) throws Exception {
        try {
            List<ClienteRutinaDTO> clienteRutinaDTOs = new ArrayList<ClienteRutinaDTO>();

            for (ClienteRutina clienteRutina : listClienteRutina) {
                ClienteRutinaDTO clienteRutinaDTO = clienteRutinaToClienteRutinaDTO(clienteRutina);

                clienteRutinaDTOs.add(clienteRutinaDTO);
            }

            return clienteRutinaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ClienteRutina> listClienteRutinaDTOToListClienteRutina(
        List<ClienteRutinaDTO> listClienteRutinaDTO) throws Exception {
        try {
            List<ClienteRutina> listClienteRutina = new ArrayList<ClienteRutina>();

            for (ClienteRutinaDTO clienteRutinaDTO : listClienteRutinaDTO) {
                ClienteRutina clienteRutina = clienteRutinaDTOToClienteRutina(clienteRutinaDTO);

                listClienteRutina.add(clienteRutina);
            }

            return listClienteRutina;
        } catch (Exception e) {
            throw e;
        }
    }
}
