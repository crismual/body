package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class EjercicioMapper implements IEjercicioMapper {
    private static final Logger log = LoggerFactory.getLogger(EjercicioMapper.class);

    /**
    * Logic injected by Spring that manages TipoEjercicio entities
    *
    */
    @Autowired
    ITipoEjercicioLogic logicTipoEjercicio1;

    @Transactional(readOnly = true)
    public EjercicioDTO ejercicioToEjercicioDTO(Ejercicio ejercicio)
        throws Exception {
        try {
            EjercicioDTO ejercicioDTO = new EjercicioDTO();

            ejercicioDTO.setEjeId(ejercicio.getEjeId());
            ejercicioDTO.setActivo((ejercicio.getActivo() != null)
                ? ejercicio.getActivo() : null);
            ejercicioDTO.setDescripcion((ejercicio.getDescripcion() != null)
                ? ejercicio.getDescripcion() : null);
            ejercicioDTO.setDia((ejercicio.getDia() != null)
                ? ejercicio.getDia() : null);
            ejercicioDTO.setFechaCreacion(ejercicio.getFechaCreacion());
            ejercicioDTO.setFechaModificacion(ejercicio.getFechaModificacion());
            ejercicioDTO.setImagen((ejercicio.getImagen() != null)
                ? ejercicio.getImagen() : null);
            ejercicioDTO.setNombre((ejercicio.getNombre() != null)
                ? ejercicio.getNombre() : null);
            ejercicioDTO.setRepeticiones((ejercicio.getRepeticiones() != null)
                ? ejercicio.getRepeticiones() : null);
            ejercicioDTO.setSeries((ejercicio.getSeries() != null)
                ? ejercicio.getSeries() : null);
            ejercicioDTO.setUsuarioCreador((ejercicio.getUsuarioCreador() != null)
                ? ejercicio.getUsuarioCreador() : null);
            ejercicioDTO.setUsuarioModificador((ejercicio.getUsuarioModificador() != null)
                ? ejercicio.getUsuarioModificador() : null);
            ejercicioDTO.setTiejeId_TipoEjercicio((ejercicio.getTipoEjercicio()
                                                            .getTiejeId() != null)
                ? ejercicio.getTipoEjercicio().getTiejeId() : null);

            return ejercicioDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Ejercicio ejercicioDTOToEjercicio(EjercicioDTO ejercicioDTO)
        throws Exception {
        try {
            Ejercicio ejercicio = new Ejercicio();

            ejercicio.setEjeId(ejercicioDTO.getEjeId());
            ejercicio.setActivo((ejercicioDTO.getActivo() != null)
                ? ejercicioDTO.getActivo() : null);
            ejercicio.setDescripcion((ejercicioDTO.getDescripcion() != null)
                ? ejercicioDTO.getDescripcion() : null);
            ejercicio.setDia((ejercicioDTO.getDia() != null)
                ? ejercicioDTO.getDia() : null);
            ejercicio.setFechaCreacion(ejercicioDTO.getFechaCreacion());
            ejercicio.setFechaModificacion(ejercicioDTO.getFechaModificacion());
            ejercicio.setImagen((ejercicioDTO.getImagen() != null)
                ? ejercicioDTO.getImagen() : null);
            ejercicio.setNombre((ejercicioDTO.getNombre() != null)
                ? ejercicioDTO.getNombre() : null);
            ejercicio.setRepeticiones((ejercicioDTO.getRepeticiones() != null)
                ? ejercicioDTO.getRepeticiones() : null);
            ejercicio.setSeries((ejercicioDTO.getSeries() != null)
                ? ejercicioDTO.getSeries() : null);
            ejercicio.setUsuarioCreador((ejercicioDTO.getUsuarioCreador() != null)
                ? ejercicioDTO.getUsuarioCreador() : null);
            ejercicio.setUsuarioModificador((ejercicioDTO.getUsuarioModificador() != null)
                ? ejercicioDTO.getUsuarioModificador() : null);

            TipoEjercicio tipoEjercicio = new TipoEjercicio();

            if (ejercicioDTO.getTiejeId_TipoEjercicio() != null) {
                tipoEjercicio = logicTipoEjercicio1.getTipoEjercicio(ejercicioDTO.getTiejeId_TipoEjercicio());
            }

            if (tipoEjercicio != null) {
                ejercicio.setTipoEjercicio(tipoEjercicio);
            }

            return ejercicio;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<EjercicioDTO> listEjercicioToListEjercicioDTO(
        List<Ejercicio> listEjercicio) throws Exception {
        try {
            List<EjercicioDTO> ejercicioDTOs = new ArrayList<EjercicioDTO>();

            for (Ejercicio ejercicio : listEjercicio) {
                EjercicioDTO ejercicioDTO = ejercicioToEjercicioDTO(ejercicio);

                ejercicioDTOs.add(ejercicioDTO);
            }

            return ejercicioDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Ejercicio> listEjercicioDTOToListEjercicio(
        List<EjercicioDTO> listEjercicioDTO) throws Exception {
        try {
            List<Ejercicio> listEjercicio = new ArrayList<Ejercicio>();

            for (EjercicioDTO ejercicioDTO : listEjercicioDTO) {
                Ejercicio ejercicio = ejercicioDTOToEjercicio(ejercicioDTO);

                listEjercicio.add(ejercicio);
            }

            return listEjercicio;
        } catch (Exception e) {
            throw e;
        }
    }
}
