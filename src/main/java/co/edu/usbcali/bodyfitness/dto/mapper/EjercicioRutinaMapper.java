package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioRutinaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class EjercicioRutinaMapper implements IEjercicioRutinaMapper {
    private static final Logger log = LoggerFactory.getLogger(EjercicioRutinaMapper.class);

    /**
    * Logic injected by Spring that manages Ejercicio entities
    *
    */
    @Autowired
    IEjercicioLogic logicEjercicio1;

    /**
    * Logic injected by Spring that manages Rutina entities
    *
    */
    @Autowired
    IRutinaLogic logicRutina2;

    @Transactional(readOnly = true)
    public EjercicioRutinaDTO ejercicioRutinaToEjercicioRutinaDTO(
        EjercicioRutina ejercicioRutina) throws Exception {
        try {
            EjercicioRutinaDTO ejercicioRutinaDTO = new EjercicioRutinaDTO();

            ejercicioRutinaDTO.setEjerutId(ejercicioRutina.getEjerutId());
            ejercicioRutinaDTO.setActivo((ejercicioRutina.getActivo() != null)
                ? ejercicioRutina.getActivo() : null);
            ejercicioRutinaDTO.setFechaCreacion(ejercicioRutina.getFechaCreacion());
            ejercicioRutinaDTO.setFechaModificacion(ejercicioRutina.getFechaModificacion());
            ejercicioRutinaDTO.setUsuarioCreador((ejercicioRutina.getUsuarioCreador() != null)
                ? ejercicioRutina.getUsuarioCreador() : null);
            ejercicioRutinaDTO.setUsuarioModificador((ejercicioRutina.getUsuarioModificador() != null)
                ? ejercicioRutina.getUsuarioModificador() : null);

            if (ejercicioRutina.getEjercicio() != null) {
                ejercicioRutinaDTO.setEjeId_Ejercicio(ejercicioRutina.getEjercicio()
                                                                     .getEjeId());
            } else {
                ejercicioRutinaDTO.setEjeId_Ejercicio(null);
            }

            if (ejercicioRutina.getRutina() != null) {
                ejercicioRutinaDTO.setRutId_Rutina(ejercicioRutina.getRutina()
                                                                  .getRutId());
            } else {
                ejercicioRutinaDTO.setRutId_Rutina(null);
            }

            return ejercicioRutinaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public EjercicioRutina ejercicioRutinaDTOToEjercicioRutina(
        EjercicioRutinaDTO ejercicioRutinaDTO) throws Exception {
        try {
            EjercicioRutina ejercicioRutina = new EjercicioRutina();

            ejercicioRutina.setEjerutId(ejercicioRutinaDTO.getEjerutId());
            ejercicioRutina.setActivo((ejercicioRutinaDTO.getActivo() != null)
                ? ejercicioRutinaDTO.getActivo() : null);
            ejercicioRutina.setFechaCreacion(ejercicioRutinaDTO.getFechaCreacion());
            ejercicioRutina.setFechaModificacion(ejercicioRutinaDTO.getFechaModificacion());
            ejercicioRutina.setUsuarioCreador((ejercicioRutinaDTO.getUsuarioCreador() != null)
                ? ejercicioRutinaDTO.getUsuarioCreador() : null);
            ejercicioRutina.setUsuarioModificador((ejercicioRutinaDTO.getUsuarioModificador() != null)
                ? ejercicioRutinaDTO.getUsuarioModificador() : null);

            Ejercicio ejercicio = new Ejercicio();

            if (ejercicioRutinaDTO.getEjeId_Ejercicio() != null) {
                ejercicio = logicEjercicio1.getEjercicio(ejercicioRutinaDTO.getEjeId_Ejercicio());
            }

            if (ejercicio != null) {
                ejercicioRutina.setEjercicio(ejercicio);
            }

            Rutina rutina = new Rutina();

            if (ejercicioRutinaDTO.getRutId_Rutina() != null) {
                rutina = logicRutina2.getRutina(ejercicioRutinaDTO.getRutId_Rutina());
            }

            if (rutina != null) {
                ejercicioRutina.setRutina(rutina);
            }

            return ejercicioRutina;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<EjercicioRutinaDTO> listEjercicioRutinaToListEjercicioRutinaDTO(
        List<EjercicioRutina> listEjercicioRutina) throws Exception {
        try {
            List<EjercicioRutinaDTO> ejercicioRutinaDTOs = new ArrayList<EjercicioRutinaDTO>();

            for (EjercicioRutina ejercicioRutina : listEjercicioRutina) {
                EjercicioRutinaDTO ejercicioRutinaDTO = ejercicioRutinaToEjercicioRutinaDTO(ejercicioRutina);

                ejercicioRutinaDTOs.add(ejercicioRutinaDTO);
            }

            return ejercicioRutinaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<EjercicioRutina> listEjercicioRutinaDTOToListEjercicioRutina(
        List<EjercicioRutinaDTO> listEjercicioRutinaDTO)
        throws Exception {
        try {
            List<EjercicioRutina> listEjercicioRutina = new ArrayList<EjercicioRutina>();

            for (EjercicioRutinaDTO ejercicioRutinaDTO : listEjercicioRutinaDTO) {
                EjercicioRutina ejercicioRutina = ejercicioRutinaDTOToEjercicioRutina(ejercicioRutinaDTO);

                listEjercicioRutina.add(ejercicioRutina);
            }

            return listEjercicioRutina;
        } catch (Exception e) {
            throw e;
        }
    }
}
