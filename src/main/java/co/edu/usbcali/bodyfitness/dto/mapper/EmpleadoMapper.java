package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Empleado;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class EmpleadoMapper implements IEmpleadoMapper {
    private static final Logger log = LoggerFactory.getLogger(EmpleadoMapper.class);

    /**
    * Logic injected by Spring that manages TipoEmpleado entities
    *
    */
    @Autowired
    ITipoEmpleadoLogic logicTipoEmpleado1;

    @Transactional(readOnly = true)
    public EmpleadoDTO empleadoToEmpleadoDTO(Empleado empleado)
        throws Exception {
        try {
            EmpleadoDTO empleadoDTO = new EmpleadoDTO();

            empleadoDTO.setEmpId(empleado.getEmpId());
            empleadoDTO.setActivo((empleado.getActivo() != null)
                ? empleado.getActivo() : null);
            empleadoDTO.setApellido((empleado.getApellido() != null)
                ? empleado.getApellido() : null);
            empleadoDTO.setCedula((empleado.getCedula() != null)
                ? empleado.getCedula() : null);
            empleadoDTO.setCelular((empleado.getCelular() != null)
                ? empleado.getCelular() : null);
            empleadoDTO.setCorreo((empleado.getCorreo() != null)
                ? empleado.getCorreo() : null);
            empleadoDTO.setDireccion((empleado.getDireccion() != null)
                ? empleado.getDireccion() : null);
            empleadoDTO.setFechaCreacion(empleado.getFechaCreacion());
            empleadoDTO.setFechaModificacion(empleado.getFechaModificacion());
            empleadoDTO.setFechaNacimiento(empleado.getFechaNacimiento());
            empleadoDTO.setFoto((empleado.getFoto() != null)
                ? empleado.getFoto() : null);
            empleadoDTO.setNombre((empleado.getNombre() != null)
                ? empleado.getNombre() : null);
            empleadoDTO.setPassword((empleado.getPassword() != null)
                ? empleado.getPassword() : null);
            empleadoDTO.setSexo((empleado.getSexo() != null)
                ? empleado.getSexo() : null);
            empleadoDTO.setUsuarioCreador((empleado.getUsuarioCreador() != null)
                ? empleado.getUsuarioCreador() : null);
            empleadoDTO.setUsuarioModificador((empleado.getUsuarioModificador() != null)
                ? empleado.getUsuarioModificador() : null);
            empleadoDTO.setTiemId_TipoEmpleado((empleado.getTipoEmpleado()
                                                        .getTiemId() != null)
                ? empleado.getTipoEmpleado().getTiemId() : null);

            return empleadoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Empleado empleadoDTOToEmpleado(EmpleadoDTO empleadoDTO)
        throws Exception {
        try {
            Empleado empleado = new Empleado();

            empleado.setEmpId(empleadoDTO.getEmpId());
            empleado.setActivo((empleadoDTO.getActivo() != null)
                ? empleadoDTO.getActivo() : null);
            empleado.setApellido((empleadoDTO.getApellido() != null)
                ? empleadoDTO.getApellido() : null);
            empleado.setCedula((empleadoDTO.getCedula() != null)
                ? empleadoDTO.getCedula() : null);
            empleado.setCelular((empleadoDTO.getCelular() != null)
                ? empleadoDTO.getCelular() : null);
            empleado.setCorreo((empleadoDTO.getCorreo() != null)
                ? empleadoDTO.getCorreo() : null);
            empleado.setDireccion((empleadoDTO.getDireccion() != null)
                ? empleadoDTO.getDireccion() : null);
            empleado.setFechaCreacion(empleadoDTO.getFechaCreacion());
            empleado.setFechaModificacion(empleadoDTO.getFechaModificacion());
            empleado.setFechaNacimiento(empleadoDTO.getFechaNacimiento());
            empleado.setFoto((empleadoDTO.getFoto() != null)
                ? empleadoDTO.getFoto() : null);
            empleado.setNombre((empleadoDTO.getNombre() != null)
                ? empleadoDTO.getNombre() : null);
            empleado.setPassword((empleadoDTO.getPassword() != null)
                ? empleadoDTO.getPassword() : null);
            empleado.setSexo((empleadoDTO.getSexo() != null)
                ? empleadoDTO.getSexo() : null);
            empleado.setUsuarioCreador((empleadoDTO.getUsuarioCreador() != null)
                ? empleadoDTO.getUsuarioCreador() : null);
            empleado.setUsuarioModificador((empleadoDTO.getUsuarioModificador() != null)
                ? empleadoDTO.getUsuarioModificador() : null);

            TipoEmpleado tipoEmpleado = new TipoEmpleado();

            if (empleadoDTO.getTiemId_TipoEmpleado() != null) {
                tipoEmpleado = logicTipoEmpleado1.getTipoEmpleado(empleadoDTO.getTiemId_TipoEmpleado());
            }

            if (tipoEmpleado != null) {
                empleado.setTipoEmpleado(tipoEmpleado);
            }

            return empleado;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<EmpleadoDTO> listEmpleadoToListEmpleadoDTO(
        List<Empleado> listEmpleado) throws Exception {
        try {
            List<EmpleadoDTO> empleadoDTOs = new ArrayList<EmpleadoDTO>();

            for (Empleado empleado : listEmpleado) {
                EmpleadoDTO empleadoDTO = empleadoToEmpleadoDTO(empleado);

                empleadoDTOs.add(empleadoDTO);
            }

            return empleadoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Empleado> listEmpleadoDTOToListEmpleado(
        List<EmpleadoDTO> listEmpleadoDTO) throws Exception {
        try {
            List<Empleado> listEmpleado = new ArrayList<Empleado>();

            for (EmpleadoDTO empleadoDTO : listEmpleadoDTO) {
                Empleado empleado = empleadoDTOToEmpleado(empleadoDTO);

                listEmpleado.add(empleado);
            }

            return listEmpleado;
        } catch (Exception e) {
            throw e;
        }
    }
}
