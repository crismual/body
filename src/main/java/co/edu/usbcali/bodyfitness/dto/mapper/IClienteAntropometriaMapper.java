package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IClienteAntropometriaMapper {
    public ClienteAntropometriaDTO clienteAntropometriaToClienteAntropometriaDTO(
        ClienteAntropometria clienteAntropometria) throws Exception;

    public ClienteAntropometria clienteAntropometriaDTOToClienteAntropometria(
        ClienteAntropometriaDTO clienteAntropometriaDTO)
        throws Exception;

    public List<ClienteAntropometriaDTO> listClienteAntropometriaToListClienteAntropometriaDTO(
        List<ClienteAntropometria> clienteAntropometrias)
        throws Exception;

    public List<ClienteAntropometria> listClienteAntropometriaDTOToListClienteAntropometria(
        List<ClienteAntropometriaDTO> clienteAntropometriaDTOs)
        throws Exception;
}
