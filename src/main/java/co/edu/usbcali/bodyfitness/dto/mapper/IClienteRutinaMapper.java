package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IClienteRutinaMapper {
    public ClienteRutinaDTO clienteRutinaToClienteRutinaDTO(
        ClienteRutina clienteRutina) throws Exception;

    public ClienteRutina clienteRutinaDTOToClienteRutina(
        ClienteRutinaDTO clienteRutinaDTO) throws Exception;

    public List<ClienteRutinaDTO> listClienteRutinaToListClienteRutinaDTO(
        List<ClienteRutina> clienteRutinas) throws Exception;

    public List<ClienteRutina> listClienteRutinaDTOToListClienteRutina(
        List<ClienteRutinaDTO> clienteRutinaDTOs) throws Exception;
}
