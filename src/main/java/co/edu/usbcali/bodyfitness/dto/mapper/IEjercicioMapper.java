package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEjercicioMapper {
    public EjercicioDTO ejercicioToEjercicioDTO(Ejercicio ejercicio)
        throws Exception;

    public Ejercicio ejercicioDTOToEjercicio(EjercicioDTO ejercicioDTO)
        throws Exception;

    public List<EjercicioDTO> listEjercicioToListEjercicioDTO(
        List<Ejercicio> ejercicios) throws Exception;

    public List<Ejercicio> listEjercicioDTOToListEjercicio(
        List<EjercicioDTO> ejercicioDTOs) throws Exception;
}
