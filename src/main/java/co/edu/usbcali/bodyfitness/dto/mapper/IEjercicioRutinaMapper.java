package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioRutinaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEjercicioRutinaMapper {
    public EjercicioRutinaDTO ejercicioRutinaToEjercicioRutinaDTO(
        EjercicioRutina ejercicioRutina) throws Exception;

    public EjercicioRutina ejercicioRutinaDTOToEjercicioRutina(
        EjercicioRutinaDTO ejercicioRutinaDTO) throws Exception;

    public List<EjercicioRutinaDTO> listEjercicioRutinaToListEjercicioRutinaDTO(
        List<EjercicioRutina> ejercicioRutinas) throws Exception;

    public List<EjercicioRutina> listEjercicioRutinaDTOToListEjercicioRutina(
        List<EjercicioRutinaDTO> ejercicioRutinaDTOs) throws Exception;
}
