package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.Empleado;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEmpleadoMapper {
    public EmpleadoDTO empleadoToEmpleadoDTO(Empleado empleado)
        throws Exception;

    public Empleado empleadoDTOToEmpleado(EmpleadoDTO empleadoDTO)
        throws Exception;

    public List<EmpleadoDTO> listEmpleadoToListEmpleadoDTO(
        List<Empleado> empleados) throws Exception;

    public List<Empleado> listEmpleadoDTOToListEmpleado(
        List<EmpleadoDTO> empleadoDTOs) throws Exception;
}
