package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IPagoMapper {
    public PagoDTO pagoToPagoDTO(Pago pago) throws Exception;

    public Pago pagoDTOToPago(PagoDTO pagoDTO) throws Exception;

    public List<PagoDTO> listPagoToListPagoDTO(List<Pago> pagos)
        throws Exception;

    public List<Pago> listPagoDTOToListPago(List<PagoDTO> pagoDTOs)
        throws Exception;
}
