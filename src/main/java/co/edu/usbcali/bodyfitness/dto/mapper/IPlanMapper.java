package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IPlanMapper {
    public PlanDTO planToPlanDTO(Plan plan) throws Exception;

    public Plan planDTOToPlan(PlanDTO planDTO) throws Exception;

    public List<PlanDTO> listPlanToListPlanDTO(List<Plan> plans)
        throws Exception;

    public List<Plan> listPlanDTOToListPlan(List<PlanDTO> planDTOs)
        throws Exception;
}
