package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.Rutina;
import co.edu.usbcali.bodyfitness.modelo.dto.RutinaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IRutinaMapper {
    public RutinaDTO rutinaToRutinaDTO(Rutina rutina) throws Exception;

    public Rutina rutinaDTOToRutina(RutinaDTO rutinaDTO)
        throws Exception;

    public List<RutinaDTO> listRutinaToListRutinaDTO(List<Rutina> rutinas)
        throws Exception;

    public List<Rutina> listRutinaDTOToListRutina(List<RutinaDTO> rutinaDTOs)
        throws Exception;
}
