package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoPagoDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ITipoPagoMapper {
    public TipoPagoDTO tipoPagoToTipoPagoDTO(TipoPago tipoPago)
        throws Exception;

    public TipoPago tipoPagoDTOToTipoPago(TipoPagoDTO tipoPagoDTO)
        throws Exception;

    public List<TipoPagoDTO> listTipoPagoToListTipoPagoDTO(
        List<TipoPago> tipoPagos) throws Exception;

    public List<TipoPago> listTipoPagoDTOToListTipoPago(
        List<TipoPagoDTO> tipoPagoDTOs) throws Exception;
}
