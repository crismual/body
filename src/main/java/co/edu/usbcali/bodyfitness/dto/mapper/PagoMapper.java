package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PagoMapper implements IPagoMapper {
    private static final Logger log = LoggerFactory.getLogger(PagoMapper.class);

    /**
    * Logic injected by Spring that manages PlanCliente entities
    *
    */
    @Autowired
    IPlanClienteLogic logicPlanCliente1;

    /**
    * Logic injected by Spring that manages TipoPago entities
    *
    */
    @Autowired
    ITipoPagoLogic logicTipoPago2;

    @Transactional(readOnly = true)
    public PagoDTO pagoToPagoDTO(Pago pago) throws Exception {
        try {
            PagoDTO pagoDTO = new PagoDTO();

            pagoDTO.setPagId(pago.getPagId());
            pagoDTO.setActivo((pago.getActivo() != null) ? pago.getActivo() : null);
            pagoDTO.setFechaCreacion(pago.getFechaCreacion());
            pagoDTO.setFechaModificacion(pago.getFechaModificacion());
            pagoDTO.setFechaPago(pago.getFechaPago());
            pagoDTO.setUsuarioCreador((pago.getUsuarioCreador() != null)
                ? pago.getUsuarioCreador() : null);
            pagoDTO.setUsuarioModificador((pago.getUsuarioModificador() != null)
                ? pago.getUsuarioModificador() : null);
            pagoDTO.setValor((pago.getValor() != null) ? pago.getValor() : null);
            pagoDTO.setPlancliId_PlanCliente((pago.getPlanCliente()
                                                  .getPlancliId() != null)
                ? pago.getPlanCliente().getPlancliId() : null);
            pagoDTO.setTipaId_TipoPago((pago.getTipoPago().getTipaId() != null)
                ? pago.getTipoPago().getTipaId() : null);

            return pagoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Pago pagoDTOToPago(PagoDTO pagoDTO) throws Exception {
        try {
            Pago pago = new Pago();

            pago.setPagId(pagoDTO.getPagId());
            pago.setActivo((pagoDTO.getActivo() != null) ? pagoDTO.getActivo()
                                                         : null);
            pago.setFechaCreacion(pagoDTO.getFechaCreacion());
            pago.setFechaModificacion(pagoDTO.getFechaModificacion());
            pago.setFechaPago(pagoDTO.getFechaPago());
            pago.setUsuarioCreador((pagoDTO.getUsuarioCreador() != null)
                ? pagoDTO.getUsuarioCreador() : null);
            pago.setUsuarioModificador((pagoDTO.getUsuarioModificador() != null)
                ? pagoDTO.getUsuarioModificador() : null);
            pago.setValor((pagoDTO.getValor() != null) ? pagoDTO.getValor() : null);

            PlanCliente planCliente = new PlanCliente();

            if (pagoDTO.getPlancliId_PlanCliente() != null) {
                planCliente = logicPlanCliente1.getPlanCliente(pagoDTO.getPlancliId_PlanCliente());
            }

            if (planCliente != null) {
                pago.setPlanCliente(planCliente);
            }

            TipoPago tipoPago = new TipoPago();

            if (pagoDTO.getTipaId_TipoPago() != null) {
                tipoPago = logicTipoPago2.getTipoPago(pagoDTO.getTipaId_TipoPago());
            }

            if (tipoPago != null) {
                pago.setTipoPago(tipoPago);
            }

            return pago;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PagoDTO> listPagoToListPagoDTO(List<Pago> listPago)
        throws Exception {
        try {
            List<PagoDTO> pagoDTOs = new ArrayList<PagoDTO>();

            for (Pago pago : listPago) {
                PagoDTO pagoDTO = pagoToPagoDTO(pago);

                pagoDTOs.add(pagoDTO);
            }

            return pagoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Pago> listPagoDTOToListPago(List<PagoDTO> listPagoDTO)
        throws Exception {
        try {
            List<Pago> listPago = new ArrayList<Pago>();

            for (PagoDTO pagoDTO : listPagoDTO) {
                Pago pago = pagoDTOToPago(pagoDTO);

                listPago.add(pago);
            }

            return listPago;
        } catch (Exception e) {
            throw e;
        }
    }
}
