package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanClienteDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PlanClienteMapper implements IPlanClienteMapper {
    private static final Logger log = LoggerFactory.getLogger(PlanClienteMapper.class);

    /**
    * Logic injected by Spring that manages Cliente entities
    *
    */
    @Autowired
    IClienteLogic logicCliente1;

    /**
    * Logic injected by Spring that manages Plan entities
    *
    */
    @Autowired
    IPlanLogic logicPlan2;

    @Transactional(readOnly = true)
    public PlanClienteDTO planClienteToPlanClienteDTO(PlanCliente planCliente)
        throws Exception {
        try {
            PlanClienteDTO planClienteDTO = new PlanClienteDTO();

            planClienteDTO.setPlancliId(planCliente.getPlancliId());
            planClienteDTO.setActivo((planCliente.getActivo() != null)
                ? planCliente.getActivo() : null);
            planClienteDTO.setFechaCreacion(planCliente.getFechaCreacion());
            planClienteDTO.setFechaModificacion(planCliente.getFechaModificacion());
            planClienteDTO.setUsuarioCreador((planCliente.getUsuarioCreador() != null)
                ? planCliente.getUsuarioCreador() : null);
            planClienteDTO.setUsuarioModificador((planCliente.getUsuarioModificador() != null)
                ? planCliente.getUsuarioModificador() : null);

            if (planCliente.getCliente() != null) {
                planClienteDTO.setCliId_Cliente(planCliente.getCliente()
                                                           .getCliId());
            } else {
                planClienteDTO.setCliId_Cliente(null);
            }

            if (planCliente.getPlan() != null) {
                planClienteDTO.setPlaId_Plan(planCliente.getPlan().getPlaId());
            } else {
                planClienteDTO.setPlaId_Plan(null);
            }

            return planClienteDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public PlanCliente planClienteDTOToPlanCliente(
        PlanClienteDTO planClienteDTO) throws Exception {
        try {
            PlanCliente planCliente = new PlanCliente();

            planCliente.setPlancliId(planClienteDTO.getPlancliId());
            planCliente.setActivo((planClienteDTO.getActivo() != null)
                ? planClienteDTO.getActivo() : null);
            planCliente.setFechaCreacion(planClienteDTO.getFechaCreacion());
            planCliente.setFechaModificacion(planClienteDTO.getFechaModificacion());
            planCliente.setUsuarioCreador((planClienteDTO.getUsuarioCreador() != null)
                ? planClienteDTO.getUsuarioCreador() : null);
            planCliente.setUsuarioModificador((planClienteDTO.getUsuarioModificador() != null)
                ? planClienteDTO.getUsuarioModificador() : null);

            Cliente cliente = new Cliente();

            if (planClienteDTO.getCliId_Cliente() != null) {
                cliente = logicCliente1.getCliente(planClienteDTO.getCliId_Cliente());
            }

            if (cliente != null) {
                planCliente.setCliente(cliente);
            }

            Plan plan = new Plan();

            if (planClienteDTO.getPlaId_Plan() != null) {
                plan = logicPlan2.getPlan(planClienteDTO.getPlaId_Plan());
            }

            if (plan != null) {
                planCliente.setPlan(plan);
            }

            return planCliente;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PlanClienteDTO> listPlanClienteToListPlanClienteDTO(
        List<PlanCliente> listPlanCliente) throws Exception {
        try {
            List<PlanClienteDTO> planClienteDTOs = new ArrayList<PlanClienteDTO>();

            for (PlanCliente planCliente : listPlanCliente) {
                PlanClienteDTO planClienteDTO = planClienteToPlanClienteDTO(planCliente);

                planClienteDTOs.add(planClienteDTO);
            }

            return planClienteDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PlanCliente> listPlanClienteDTOToListPlanCliente(
        List<PlanClienteDTO> listPlanClienteDTO) throws Exception {
        try {
            List<PlanCliente> listPlanCliente = new ArrayList<PlanCliente>();

            for (PlanClienteDTO planClienteDTO : listPlanClienteDTO) {
                PlanCliente planCliente = planClienteDTOToPlanCliente(planClienteDTO);

                listPlanCliente.add(planCliente);
            }

            return listPlanCliente;
        } catch (Exception e) {
            throw e;
        }
    }
}
