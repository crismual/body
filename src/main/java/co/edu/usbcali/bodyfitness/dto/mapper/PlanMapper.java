package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PlanMapper implements IPlanMapper {
    private static final Logger log = LoggerFactory.getLogger(PlanMapper.class);

    @Transactional(readOnly = true)
    public PlanDTO planToPlanDTO(Plan plan) throws Exception {
        try {
            PlanDTO planDTO = new PlanDTO();

            planDTO.setPlaId(plan.getPlaId());
            planDTO.setActivo((plan.getActivo() != null) ? plan.getActivo() : null);
            planDTO.setDescripcion((plan.getDescripcion() != null)
                ? plan.getDescripcion() : null);
            planDTO.setDias((plan.getDias() != null) ? plan.getDias() : null);
            planDTO.setFechaCreacion(plan.getFechaCreacion());
            planDTO.setFechaModificacion(plan.getFechaModificacion());
            planDTO.setMesFin(plan.getMesFin());
            planDTO.setMesInicio(plan.getMesInicio());
            planDTO.setNombre((plan.getNombre() != null) ? plan.getNombre() : null);
            planDTO.setUsuarioCreador((plan.getUsuarioCreador() != null)
                ? plan.getUsuarioCreador() : null);
            planDTO.setUsuarioModificador((plan.getUsuarioModificador() != null)
                ? plan.getUsuarioModificador() : null);
            planDTO.setValor((plan.getValor() != null) ? plan.getValor() : null);

            return planDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Plan planDTOToPlan(PlanDTO planDTO) throws Exception {
        try {
            Plan plan = new Plan();

            plan.setPlaId(planDTO.getPlaId());
            plan.setActivo((planDTO.getActivo() != null) ? planDTO.getActivo()
                                                         : null);
            plan.setDescripcion((planDTO.getDescripcion() != null)
                ? planDTO.getDescripcion() : null);
            plan.setDias((planDTO.getDias() != null) ? planDTO.getDias() : null);
            plan.setFechaCreacion(planDTO.getFechaCreacion());
            plan.setFechaModificacion(planDTO.getFechaModificacion());
            plan.setMesFin(planDTO.getMesFin());
            plan.setMesInicio(planDTO.getMesInicio());
            plan.setNombre((planDTO.getNombre() != null) ? planDTO.getNombre()
                                                         : null);
            plan.setUsuarioCreador((planDTO.getUsuarioCreador() != null)
                ? planDTO.getUsuarioCreador() : null);
            plan.setUsuarioModificador((planDTO.getUsuarioModificador() != null)
                ? planDTO.getUsuarioModificador() : null);
            plan.setValor((planDTO.getValor() != null) ? planDTO.getValor() : null);

            return plan;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PlanDTO> listPlanToListPlanDTO(List<Plan> listPlan)
        throws Exception {
        try {
            List<PlanDTO> planDTOs = new ArrayList<PlanDTO>();

            for (Plan plan : listPlan) {
                PlanDTO planDTO = planToPlanDTO(plan);

                planDTOs.add(planDTO);
            }

            return planDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Plan> listPlanDTOToListPlan(List<PlanDTO> listPlanDTO)
        throws Exception {
        try {
            List<Plan> listPlan = new ArrayList<Plan>();

            for (PlanDTO planDTO : listPlanDTO) {
                Plan plan = planDTOToPlan(planDTO);

                listPlan.add(plan);
            }

            return listPlan;
        } catch (Exception e) {
            throw e;
        }
    }
}
