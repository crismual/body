package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.Rutina;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.RutinaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class RutinaMapper implements IRutinaMapper {
    private static final Logger log = LoggerFactory.getLogger(RutinaMapper.class);

    /**
    * Logic injected by Spring that manages Empleado entities
    *
    */
    @Autowired
    IEmpleadoLogic logicEmpleado1;

    @Transactional(readOnly = true)
    public RutinaDTO rutinaToRutinaDTO(Rutina rutina) throws Exception {
        try {
            RutinaDTO rutinaDTO = new RutinaDTO();

            rutinaDTO.setRutId(rutina.getRutId());
            rutinaDTO.setActivo((rutina.getActivo() != null)
                ? rutina.getActivo() : null);
            rutinaDTO.setFechaCreacion(rutina.getFechaCreacion());
            rutinaDTO.setFechaModificacion(rutina.getFechaModificacion());
            rutinaDTO.setNombre((rutina.getNombre() != null)
                ? rutina.getNombre() : null);
            rutinaDTO.setUsuarioCreador((rutina.getUsuarioCreador() != null)
                ? rutina.getUsuarioCreador() : null);
            rutinaDTO.setUsuarioModificador((rutina.getUsuarioModificador() != null)
                ? rutina.getUsuarioModificador() : null);

            if (rutina.getEmpleado() != null) {
                rutinaDTO.setEmpId_Empleado(rutina.getEmpleado().getEmpId());
            } else {
                rutinaDTO.setEmpId_Empleado(null);
            }

            return rutinaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Rutina rutinaDTOToRutina(RutinaDTO rutinaDTO)
        throws Exception {
        try {
            Rutina rutina = new Rutina();

            rutina.setRutId(rutinaDTO.getRutId());
            rutina.setActivo((rutinaDTO.getActivo() != null)
                ? rutinaDTO.getActivo() : null);
            rutina.setFechaCreacion(rutinaDTO.getFechaCreacion());
            rutina.setFechaModificacion(rutinaDTO.getFechaModificacion());
            rutina.setNombre((rutinaDTO.getNombre() != null)
                ? rutinaDTO.getNombre() : null);
            rutina.setUsuarioCreador((rutinaDTO.getUsuarioCreador() != null)
                ? rutinaDTO.getUsuarioCreador() : null);
            rutina.setUsuarioModificador((rutinaDTO.getUsuarioModificador() != null)
                ? rutinaDTO.getUsuarioModificador() : null);

            Empleado empleado = new Empleado();

            if (rutinaDTO.getEmpId_Empleado() != null) {
                empleado = logicEmpleado1.getEmpleado(rutinaDTO.getEmpId_Empleado());
            }

            if (empleado != null) {
                rutina.setEmpleado(empleado);
            }

            return rutina;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<RutinaDTO> listRutinaToListRutinaDTO(List<Rutina> listRutina)
        throws Exception {
        try {
            List<RutinaDTO> rutinaDTOs = new ArrayList<RutinaDTO>();

            for (Rutina rutina : listRutina) {
                RutinaDTO rutinaDTO = rutinaToRutinaDTO(rutina);

                rutinaDTOs.add(rutinaDTO);
            }

            return rutinaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Rutina> listRutinaDTOToListRutina(List<RutinaDTO> listRutinaDTO)
        throws Exception {
        try {
            List<Rutina> listRutina = new ArrayList<Rutina>();

            for (RutinaDTO rutinaDTO : listRutinaDTO) {
                Rutina rutina = rutinaDTOToRutina(rutinaDTO);

                listRutina.add(rutina);
            }

            return listRutina;
        } catch (Exception e) {
            throw e;
        }
    }
}
