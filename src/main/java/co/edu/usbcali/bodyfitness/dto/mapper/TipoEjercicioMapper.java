package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEjercicioDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class TipoEjercicioMapper implements ITipoEjercicioMapper {
    private static final Logger log = LoggerFactory.getLogger(TipoEjercicioMapper.class);

    @Transactional(readOnly = true)
    public TipoEjercicioDTO tipoEjercicioToTipoEjercicioDTO(
        TipoEjercicio tipoEjercicio) throws Exception {
        try {
            TipoEjercicioDTO tipoEjercicioDTO = new TipoEjercicioDTO();

            tipoEjercicioDTO.setTiejeId(tipoEjercicio.getTiejeId());
            tipoEjercicioDTO.setActivo((tipoEjercicio.getActivo() != null)
                ? tipoEjercicio.getActivo() : null);
            tipoEjercicioDTO.setFechaCreacion(tipoEjercicio.getFechaCreacion());
            tipoEjercicioDTO.setFechaModificacion(tipoEjercicio.getFechaModificacion());
            tipoEjercicioDTO.setNombre((tipoEjercicio.getNombre() != null)
                ? tipoEjercicio.getNombre() : null);
            tipoEjercicioDTO.setUsuarioCreador((tipoEjercicio.getUsuarioCreador() != null)
                ? tipoEjercicio.getUsuarioCreador() : null);
            tipoEjercicioDTO.setUsuarioModificador((tipoEjercicio.getUsuarioModificador() != null)
                ? tipoEjercicio.getUsuarioModificador() : null);

            return tipoEjercicioDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public TipoEjercicio tipoEjercicioDTOToTipoEjercicio(
        TipoEjercicioDTO tipoEjercicioDTO) throws Exception {
        try {
            TipoEjercicio tipoEjercicio = new TipoEjercicio();

            tipoEjercicio.setTiejeId(tipoEjercicioDTO.getTiejeId());
            tipoEjercicio.setActivo((tipoEjercicioDTO.getActivo() != null)
                ? tipoEjercicioDTO.getActivo() : null);
            tipoEjercicio.setFechaCreacion(tipoEjercicioDTO.getFechaCreacion());
            tipoEjercicio.setFechaModificacion(tipoEjercicioDTO.getFechaModificacion());
            tipoEjercicio.setNombre((tipoEjercicioDTO.getNombre() != null)
                ? tipoEjercicioDTO.getNombre() : null);
            tipoEjercicio.setUsuarioCreador((tipoEjercicioDTO.getUsuarioCreador() != null)
                ? tipoEjercicioDTO.getUsuarioCreador() : null);
            tipoEjercicio.setUsuarioModificador((tipoEjercicioDTO.getUsuarioModificador() != null)
                ? tipoEjercicioDTO.getUsuarioModificador() : null);

            return tipoEjercicio;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoEjercicioDTO> listTipoEjercicioToListTipoEjercicioDTO(
        List<TipoEjercicio> listTipoEjercicio) throws Exception {
        try {
            List<TipoEjercicioDTO> tipoEjercicioDTOs = new ArrayList<TipoEjercicioDTO>();

            for (TipoEjercicio tipoEjercicio : listTipoEjercicio) {
                TipoEjercicioDTO tipoEjercicioDTO = tipoEjercicioToTipoEjercicioDTO(tipoEjercicio);

                tipoEjercicioDTOs.add(tipoEjercicioDTO);
            }

            return tipoEjercicioDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoEjercicio> listTipoEjercicioDTOToListTipoEjercicio(
        List<TipoEjercicioDTO> listTipoEjercicioDTO) throws Exception {
        try {
            List<TipoEjercicio> listTipoEjercicio = new ArrayList<TipoEjercicio>();

            for (TipoEjercicioDTO tipoEjercicioDTO : listTipoEjercicioDTO) {
                TipoEjercicio tipoEjercicio = tipoEjercicioDTOToTipoEjercicio(tipoEjercicioDTO);

                listTipoEjercicio.add(tipoEjercicio);
            }

            return listTipoEjercicio;
        } catch (Exception e) {
            throw e;
        }
    }
}
