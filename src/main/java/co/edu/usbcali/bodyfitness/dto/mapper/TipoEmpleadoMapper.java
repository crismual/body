package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEmpleadoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class TipoEmpleadoMapper implements ITipoEmpleadoMapper {
    private static final Logger log = LoggerFactory.getLogger(TipoEmpleadoMapper.class);

    @Transactional(readOnly = true)
    public TipoEmpleadoDTO tipoEmpleadoToTipoEmpleadoDTO(
        TipoEmpleado tipoEmpleado) throws Exception {
        try {
            TipoEmpleadoDTO tipoEmpleadoDTO = new TipoEmpleadoDTO();

            tipoEmpleadoDTO.setTiemId(tipoEmpleado.getTiemId());
            tipoEmpleadoDTO.setActivo((tipoEmpleado.getActivo() != null)
                ? tipoEmpleado.getActivo() : null);
            tipoEmpleadoDTO.setFechaCreacion(tipoEmpleado.getFechaCreacion());
            tipoEmpleadoDTO.setFechaModificacion(tipoEmpleado.getFechaModificacion());
            tipoEmpleadoDTO.setNombre((tipoEmpleado.getNombre() != null)
                ? tipoEmpleado.getNombre() : null);
            tipoEmpleadoDTO.setUsuarioCreador((tipoEmpleado.getUsuarioCreador() != null)
                ? tipoEmpleado.getUsuarioCreador() : null);
            tipoEmpleadoDTO.setUsuarioModificador((tipoEmpleado.getUsuarioModificador() != null)
                ? tipoEmpleado.getUsuarioModificador() : null);

            return tipoEmpleadoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public TipoEmpleado tipoEmpleadoDTOToTipoEmpleado(
        TipoEmpleadoDTO tipoEmpleadoDTO) throws Exception {
        try {
            TipoEmpleado tipoEmpleado = new TipoEmpleado();

            tipoEmpleado.setTiemId(tipoEmpleadoDTO.getTiemId());
            tipoEmpleado.setActivo((tipoEmpleadoDTO.getActivo() != null)
                ? tipoEmpleadoDTO.getActivo() : null);
            tipoEmpleado.setFechaCreacion(tipoEmpleadoDTO.getFechaCreacion());
            tipoEmpleado.setFechaModificacion(tipoEmpleadoDTO.getFechaModificacion());
            tipoEmpleado.setNombre((tipoEmpleadoDTO.getNombre() != null)
                ? tipoEmpleadoDTO.getNombre() : null);
            tipoEmpleado.setUsuarioCreador((tipoEmpleadoDTO.getUsuarioCreador() != null)
                ? tipoEmpleadoDTO.getUsuarioCreador() : null);
            tipoEmpleado.setUsuarioModificador((tipoEmpleadoDTO.getUsuarioModificador() != null)
                ? tipoEmpleadoDTO.getUsuarioModificador() : null);

            return tipoEmpleado;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoEmpleadoDTO> listTipoEmpleadoToListTipoEmpleadoDTO(
        List<TipoEmpleado> listTipoEmpleado) throws Exception {
        try {
            List<TipoEmpleadoDTO> tipoEmpleadoDTOs = new ArrayList<TipoEmpleadoDTO>();

            for (TipoEmpleado tipoEmpleado : listTipoEmpleado) {
                TipoEmpleadoDTO tipoEmpleadoDTO = tipoEmpleadoToTipoEmpleadoDTO(tipoEmpleado);

                tipoEmpleadoDTOs.add(tipoEmpleadoDTO);
            }

            return tipoEmpleadoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoEmpleado> listTipoEmpleadoDTOToListTipoEmpleado(
        List<TipoEmpleadoDTO> listTipoEmpleadoDTO) throws Exception {
        try {
            List<TipoEmpleado> listTipoEmpleado = new ArrayList<TipoEmpleado>();

            for (TipoEmpleadoDTO tipoEmpleadoDTO : listTipoEmpleadoDTO) {
                TipoEmpleado tipoEmpleado = tipoEmpleadoDTOToTipoEmpleado(tipoEmpleadoDTO);

                listTipoEmpleado.add(tipoEmpleado);
            }

            return listTipoEmpleado;
        } catch (Exception e) {
            throw e;
        }
    }
}
