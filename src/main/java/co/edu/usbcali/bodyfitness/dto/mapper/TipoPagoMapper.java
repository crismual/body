package co.edu.usbcali.bodyfitness.dto.mapper;

import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.modelo.control.*;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoPagoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class TipoPagoMapper implements ITipoPagoMapper {
    private static final Logger log = LoggerFactory.getLogger(TipoPagoMapper.class);

    @Transactional(readOnly = true)
    public TipoPagoDTO tipoPagoToTipoPagoDTO(TipoPago tipoPago)
        throws Exception {
        try {
            TipoPagoDTO tipoPagoDTO = new TipoPagoDTO();

            tipoPagoDTO.setTipaId(tipoPago.getTipaId());
            tipoPagoDTO.setActivo((tipoPago.getActivo() != null)
                ? tipoPago.getActivo() : null);
            tipoPagoDTO.setFechaCreacion(tipoPago.getFechaCreacion());
            tipoPagoDTO.setFechaModificacion(tipoPago.getFechaModificacion());
            tipoPagoDTO.setNombre((tipoPago.getNombre() != null)
                ? tipoPago.getNombre() : null);
            tipoPagoDTO.setUsuarioCreador((tipoPago.getUsuarioCreador() != null)
                ? tipoPago.getUsuarioCreador() : null);
            tipoPagoDTO.setUsuarioModificador((tipoPago.getUsuarioModificador() != null)
                ? tipoPago.getUsuarioModificador() : null);

            return tipoPagoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public TipoPago tipoPagoDTOToTipoPago(TipoPagoDTO tipoPagoDTO)
        throws Exception {
        try {
            TipoPago tipoPago = new TipoPago();

            tipoPago.setTipaId(tipoPagoDTO.getTipaId());
            tipoPago.setActivo((tipoPagoDTO.getActivo() != null)
                ? tipoPagoDTO.getActivo() : null);
            tipoPago.setFechaCreacion(tipoPagoDTO.getFechaCreacion());
            tipoPago.setFechaModificacion(tipoPagoDTO.getFechaModificacion());
            tipoPago.setNombre((tipoPagoDTO.getNombre() != null)
                ? tipoPagoDTO.getNombre() : null);
            tipoPago.setUsuarioCreador((tipoPagoDTO.getUsuarioCreador() != null)
                ? tipoPagoDTO.getUsuarioCreador() : null);
            tipoPago.setUsuarioModificador((tipoPagoDTO.getUsuarioModificador() != null)
                ? tipoPagoDTO.getUsuarioModificador() : null);

            return tipoPago;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoPagoDTO> listTipoPagoToListTipoPagoDTO(
        List<TipoPago> listTipoPago) throws Exception {
        try {
            List<TipoPagoDTO> tipoPagoDTOs = new ArrayList<TipoPagoDTO>();

            for (TipoPago tipoPago : listTipoPago) {
                TipoPagoDTO tipoPagoDTO = tipoPagoToTipoPagoDTO(tipoPago);

                tipoPagoDTOs.add(tipoPagoDTO);
            }

            return tipoPagoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoPago> listTipoPagoDTOToListTipoPago(
        List<TipoPagoDTO> listTipoPagoDTO) throws Exception {
        try {
            List<TipoPago> listTipoPago = new ArrayList<TipoPago>();

            for (TipoPagoDTO tipoPagoDTO : listTipoPagoDTO) {
                TipoPago tipoPago = tipoPagoDTOToTipoPago(tipoPagoDTO);

                listTipoPago.add(tipoPago);
            }

            return listTipoPago;
        } catch (Exception e) {
            throw e;
        }
    }
}
