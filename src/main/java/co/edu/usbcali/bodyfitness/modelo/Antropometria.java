package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "antropometria", schema = "public")
public class Antropometria implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long antId;
    @NotNull
    private Double abdomen;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    private Double antebrazo;
    @NotNull
    private Double brazoContraido;
    @NotNull
    private Double brazoRelajado;
    @NotNull
    private Double cintura;
    @NotNull
    private Double cuello;
    @NotNull
    private Double estatura;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    private Double hombro;
    @NotNull
    private Double pantorrilla;
    @NotNull
    private Double pecho;
    @NotNull
    private Double peso;
    @NotNull
    private Double pierna;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<ClienteAntropometria> clienteAntropometrias = new HashSet<ClienteAntropometria>(0);

    public Antropometria() {
    }

    public Antropometria(Long antId, Double abdomen, String activo,
        Double antebrazo, Double brazoContraido, Double brazoRelajado,
        Double cintura, Set<ClienteAntropometria> clienteAntropometrias,
        Double cuello, Double estatura, Date fechaCreacion,
        Date fechaModificacion, Double hombro, Double pantorrilla,
        Double pecho, Double peso, Double pierna, String usuarioCreador,
        String usuarioModificador) {
        this.antId = antId;
        this.abdomen = abdomen;
        this.activo = activo;
        this.antebrazo = antebrazo;
        this.brazoContraido = brazoContraido;
        this.brazoRelajado = brazoRelajado;
        this.cintura = cintura;
        this.cuello = cuello;
        this.estatura = estatura;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.hombro = hombro;
        this.pantorrilla = pantorrilla;
        this.pecho = pecho;
        this.peso = peso;
        this.pierna = pierna;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.clienteAntropometrias = clienteAntropometrias;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ant_id", unique = true, nullable = false)
    public Long getAntId() {
        return this.antId;
    }

    public void setAntId(Long antId) {
        this.antId = antId;
    }

    @Column(name = "abdomen", nullable = false)
    public Double getAbdomen() {
        return this.abdomen;
    }

    public void setAbdomen(Double abdomen) {
        this.abdomen = abdomen;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "antebrazo", nullable = false)
    public Double getAntebrazo() {
        return this.antebrazo;
    }

    public void setAntebrazo(Double antebrazo) {
        this.antebrazo = antebrazo;
    }

    @Column(name = "brazo_contraido", nullable = false)
    public Double getBrazoContraido() {
        return this.brazoContraido;
    }

    public void setBrazoContraido(Double brazoContraido) {
        this.brazoContraido = brazoContraido;
    }

    @Column(name = "brazo_relajado", nullable = false)
    public Double getBrazoRelajado() {
        return this.brazoRelajado;
    }

    public void setBrazoRelajado(Double brazoRelajado) {
        this.brazoRelajado = brazoRelajado;
    }

    @Column(name = "cintura", nullable = false)
    public Double getCintura() {
        return this.cintura;
    }

    public void setCintura(Double cintura) {
        this.cintura = cintura;
    }

    @Column(name = "cuello", nullable = false)
    public Double getCuello() {
        return this.cuello;
    }

    public void setCuello(Double cuello) {
        this.cuello = cuello;
    }

    @Column(name = "estatura", nullable = false)
    public Double getEstatura() {
        return this.estatura;
    }

    public void setEstatura(Double estatura) {
        this.estatura = estatura;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "hombro", nullable = false)
    public Double getHombro() {
        return this.hombro;
    }

    public void setHombro(Double hombro) {
        this.hombro = hombro;
    }

    @Column(name = "pantorrilla", nullable = false)
    public Double getPantorrilla() {
        return this.pantorrilla;
    }

    public void setPantorrilla(Double pantorrilla) {
        this.pantorrilla = pantorrilla;
    }

    @Column(name = "pecho", nullable = false)
    public Double getPecho() {
        return this.pecho;
    }

    public void setPecho(Double pecho) {
        this.pecho = pecho;
    }

    @Column(name = "peso", nullable = false)
    public Double getPeso() {
        return this.peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    @Column(name = "pierna", nullable = false)
    public Double getPierna() {
        return this.pierna;
    }

    public void setPierna(Double pierna) {
        this.pierna = pierna;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "antropometria")
    public Set<ClienteAntropometria> getClienteAntropometrias() {
        return this.clienteAntropometrias;
    }

    public void setClienteAntropometrias(
        Set<ClienteAntropometria> clienteAntropometrias) {
        this.clienteAntropometrias = clienteAntropometrias;
    }
}
