package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "cliente", schema = "public")
public class Cliente implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long cliId;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String apellido;
    @NotNull
    private Long cedula;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String celular;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String correo;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String direccion;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Date fechaNacimiento;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String foto;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String nombre;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String password;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String sexo;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<ClienteAntropometria> clienteAntropometrias = new HashSet<ClienteAntropometria>(0);
    private Set<ClienteRutina> clienteRutinas = new HashSet<ClienteRutina>(0);
    private Set<PlanCliente> planClientes = new HashSet<PlanCliente>(0);

    public Cliente() {
    }

    public Cliente(Long cliId, String activo, String apellido, Long cedula,
        String celular, Set<ClienteAntropometria> clienteAntropometrias,
        Set<ClienteRutina> clienteRutinas, String correo, String direccion,
        Date fechaCreacion, Date fechaModificacion, Date fechaNacimiento,
        String foto, String nombre, String password,
        Set<PlanCliente> planClientes, String sexo, String usuarioCreador,
        String usuarioModificador) {
        this.cliId = cliId;
        this.activo = activo;
        this.apellido = apellido;
        this.cedula = cedula;
        this.celular = celular;
        this.correo = correo;
        this.direccion = direccion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.foto = foto;
        this.nombre = nombre;
        this.password = password;
        this.sexo = sexo;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.clienteAntropometrias = clienteAntropometrias;
        this.clienteRutinas = clienteRutinas;
        this.planClientes = planClientes;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cli_id", unique = true, nullable = false)
    public Long getCliId() {
        return this.cliId;
    }

    public void setCliId(Long cliId) {
        this.cliId = cliId;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "apellido", nullable = false)
    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Column(name = "cedula", nullable = false)
    public Long getCedula() {
        return this.cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    @Column(name = "celular", nullable = false)
    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Column(name = "correo", nullable = false)
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Column(name = "direccion", nullable = false)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Column(name = "foto", nullable = false)
    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "sexo", nullable = false)
    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    public Set<ClienteAntropometria> getClienteAntropometrias() {
        return this.clienteAntropometrias;
    }

    public void setClienteAntropometrias(
        Set<ClienteAntropometria> clienteAntropometrias) {
        this.clienteAntropometrias = clienteAntropometrias;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    public Set<ClienteRutina> getClienteRutinas() {
        return this.clienteRutinas;
    }

    public void setClienteRutinas(Set<ClienteRutina> clienteRutinas) {
        this.clienteRutinas = clienteRutinas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    public Set<PlanCliente> getPlanClientes() {
        return this.planClientes;
    }

    public void setPlanClientes(Set<PlanCliente> planClientes) {
        this.planClientes = planClientes;
    }
}
