package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "cliente_antropometria", schema = "public")
public class ClienteAntropometria implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long cliantId;
    @NotNull
    private Antropometria antropometria;
    @NotNull
    private Cliente cliente;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;

    public ClienteAntropometria() {
    }

    public ClienteAntropometria(Long cliantId, String activo,
        Antropometria antropometria, Cliente cliente, Date fechaCreacion,
        Date fechaModificacion, String usuarioCreador, String usuarioModificador) {
        this.cliantId = cliantId;
        this.antropometria = antropometria;
        this.cliente = cliente;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cliant_id", unique = true, nullable = false)
    public Long getCliantId() {
        return this.cliantId;
    }

    public void setCliantId(Long cliantId) {
        this.cliantId = cliantId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ant_id")
    public Antropometria getAntropometria() {
        return this.antropometria;
    }

    public void setAntropometria(Antropometria antropometria) {
        this.antropometria = antropometria;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cli_id")
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}
