package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "ejercicio_rutina", schema = "public")
public class EjercicioRutina implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long ejerutId;
    @NotNull
    private Ejercicio ejercicio;
    @NotNull
    private Rutina rutina;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;

    public EjercicioRutina() {
    }

    public EjercicioRutina(Long ejerutId, String activo, Ejercicio ejercicio,
        Date fechaCreacion, Date fechaModificacion, Rutina rutina,
        String usuarioCreador, String usuarioModificador) {
        this.ejerutId = ejerutId;
        this.ejercicio = ejercicio;
        this.rutina = rutina;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ejerut_id", unique = true, nullable = false)
    public Long getEjerutId() {
        return this.ejerutId;
    }

    public void setEjerutId(Long ejerutId) {
        this.ejerutId = ejerutId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eje_id")
    public Ejercicio getEjercicio() {
        return this.ejercicio;
    }

    public void setEjercicio(Ejercicio ejercicio) {
        this.ejercicio = ejercicio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rut_id")
    public Rutina getRutina() {
        return this.rutina;
    }

    public void setRutina(Rutina rutina) {
        this.rutina = rutina;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}
