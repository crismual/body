package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "pago", schema = "public")
public class Pago implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long pagId;
    @NotNull
    private PlanCliente planCliente;
    @NotNull
    private TipoPago tipoPago;
    private String activo;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    private Date fechaPago;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    @NotNull
    private Long valor;

    public Pago() {
    }

    public Pago(Long pagId, String activo, Date fechaCreacion,
        Date fechaModificacion, Date fechaPago, PlanCliente planCliente,
        TipoPago tipoPago, String usuarioCreador, String usuarioModificador,
        Long valor) {
        this.pagId = pagId;
        this.planCliente = planCliente;
        this.tipoPago = tipoPago;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.fechaPago = fechaPago;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.valor = valor;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pag_id", unique = true, nullable = false)
    public Long getPagId() {
        return this.pagId;
    }

    public void setPagId(Long pagId) {
        this.pagId = pagId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plancli_id")
    public PlanCliente getPlanCliente() {
        return this.planCliente;
    }

    public void setPlanCliente(PlanCliente planCliente) {
        this.planCliente = planCliente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipa_id")
    public TipoPago getTipoPago() {
        return this.tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    @Column(name = "activo")
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "fecha_pago", nullable = false)
    public Date getFechaPago() {
        return this.fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Column(name = "valor", nullable = false)
    public Long getValor() {
        return this.valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
