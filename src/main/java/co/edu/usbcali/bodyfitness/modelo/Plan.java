package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "plan", schema = "public")
public class Plan implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long plaId;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String descripcion;
    private Long dias;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Date mesFin;
    private Date mesInicio;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String nombre;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    @NotNull
    private Long valor;
    private Set<PlanCliente> planClientes = new HashSet<PlanCliente>(0);

    public Plan() {
    }

    public Plan(Long plaId, String activo, String descripcion, Long dias,
        Date fechaCreacion, Date fechaModificacion, Date mesFin,
        Date mesInicio, String nombre, Set<PlanCliente> planClientes,
        String usuarioCreador, String usuarioModificador, Long valor) {
        this.plaId = plaId;
        this.activo = activo;
        this.descripcion = descripcion;
        this.dias = dias;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.mesFin = mesFin;
        this.mesInicio = mesInicio;
        this.nombre = nombre;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.valor = valor;
        this.planClientes = planClientes;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pla_id", unique = true, nullable = false)
    public Long getPlaId() {
        return this.plaId;
    }

    public void setPlaId(Long plaId) {
        this.plaId = plaId;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "descripcion", nullable = false)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "dias")
    public Long getDias() {
        return this.dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "mes_fin")
    public Date getMesFin() {
        return this.mesFin;
    }

    public void setMesFin(Date mesFin) {
        this.mesFin = mesFin;
    }

    @Column(name = "mes_inicio")
    public Date getMesInicio() {
        return this.mesInicio;
    }

    public void setMesInicio(Date mesInicio) {
        this.mesInicio = mesInicio;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Column(name = "valor", nullable = false)
    public Long getValor() {
        return this.valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "plan")
    public Set<PlanCliente> getPlanClientes() {
        return this.planClientes;
    }

    public void setPlanClientes(Set<PlanCliente> planClientes) {
        this.planClientes = planClientes;
    }
}
