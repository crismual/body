package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "rutina", schema = "public")
public class Rutina implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long rutId;
    @NotNull
    private Empleado empleado;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String nombre;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<ClienteRutina> clienteRutinas = new HashSet<ClienteRutina>(0);
    private Set<EjercicioRutina> ejercicioRutinas = new HashSet<EjercicioRutina>(0);

    public Rutina() {
    }

    public Rutina(Long rutId, String activo, Set<ClienteRutina> clienteRutinas,
        Set<EjercicioRutina> ejercicioRutinas, Empleado empleado,
        Date fechaCreacion, Date fechaModificacion, String nombre,
        String usuarioCreador, String usuarioModificador) {
        this.rutId = rutId;
        this.empleado = empleado;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.nombre = nombre;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.clienteRutinas = clienteRutinas;
        this.ejercicioRutinas = ejercicioRutinas;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "rut_id", unique = true, nullable = false)
    public Long getRutId() {
        return this.rutId;
    }

    public void setRutId(Long rutId) {
        this.rutId = rutId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_id")
    public Empleado getEmpleado() {
        return this.empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rutina")
    public Set<ClienteRutina> getClienteRutinas() {
        return this.clienteRutinas;
    }

    public void setClienteRutinas(Set<ClienteRutina> clienteRutinas) {
        this.clienteRutinas = clienteRutinas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rutina")
    public Set<EjercicioRutina> getEjercicioRutinas() {
        return this.ejercicioRutinas;
    }

    public void setEjercicioRutinas(Set<EjercicioRutina> ejercicioRutinas) {
        this.ejercicioRutinas = ejercicioRutinas;
    }
}
