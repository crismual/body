package co.edu.usbcali.bodyfitness.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "tipo_empleado", schema = "public")
public class TipoEmpleado implements java.io.Serializable {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long tiemId;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String nombre;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<Empleado> empleados = new HashSet<Empleado>(0);

    public TipoEmpleado() {
    }

    public TipoEmpleado(Long tiemId, String activo, Set<Empleado> empleados,
        Date fechaCreacion, Date fechaModificacion, String nombre,
        String usuarioCreador, String usuarioModificador) {
        this.tiemId = tiemId;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.nombre = nombre;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.empleados = empleados;
    }

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "tiem_id", unique = true, nullable = false)
    public Long getTiemId() {
        return this.tiemId;
    }

    public void setTiemId(Long tiemId) {
        this.tiemId = tiemId;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tipoEmpleado")
    public Set<Empleado> getEmpleados() {
        return this.empleados;
    }

    public void setEmpleados(Set<Empleado> empleados) {
        this.empleados = empleados;
    }
}
