package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IClienteAntropometriaLogic {
    public List<ClienteAntropometria> getClienteAntropometria()
        throws Exception;

    /**
         * Save an new ClienteAntropometria entity
         */
    public void saveClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    /**
         * Delete an existing ClienteAntropometria entity
         *
         */
    public void deleteClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    /**
        * Update an existing ClienteAntropometria entity
        *
        */
    public void updateClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    /**
         * Load an existing ClienteAntropometria entity
         *
         */
    public ClienteAntropometria getClienteAntropometria(Long cliantId)
        throws Exception;

    public List<ClienteAntropometria> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<ClienteAntropometria> findPageClienteAntropometria(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberClienteAntropometria() throws Exception;

    public List<ClienteAntropometriaDTO> getDataClienteAntropometria()
        throws Exception;

    public void validateClienteAntropometria(
        ClienteAntropometria clienteAntropometria) throws Exception;
}
