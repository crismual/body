package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEjercicioLogic {
    public List<Ejercicio> getEjercicio() throws Exception;

    /**
         * Save an new Ejercicio entity
         */
    public void saveEjercicio(Ejercicio entity) throws Exception;

    /**
         * Delete an existing Ejercicio entity
         *
         */
    public void deleteEjercicio(Ejercicio entity) throws Exception;

    /**
        * Update an existing Ejercicio entity
        *
        */
    public void updateEjercicio(Ejercicio entity) throws Exception;

    /**
         * Load an existing Ejercicio entity
         *
         */
    public Ejercicio getEjercicio(Long ejeId) throws Exception;

    public List<Ejercicio> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Ejercicio> findPageEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberEjercicio() throws Exception;

    public List<EjercicioDTO> getDataEjercicio() throws Exception;

    public void validateEjercicio(Ejercicio ejercicio)
        throws Exception;
}
