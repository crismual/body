package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IPagoLogic {
    public List<Pago> getPago() throws Exception;

    /**
         * Save an new Pago entity
         */
    public void savePago(Pago entity) throws Exception;

    /**
         * Delete an existing Pago entity
         *
         */
    public void deletePago(Pago entity) throws Exception;

    /**
        * Update an existing Pago entity
        *
        */
    public void updatePago(Pago entity) throws Exception;

    /**
         * Load an existing Pago entity
         *
         */
    public Pago getPago(Long pagId) throws Exception;

    public List<Pago> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Pago> findPagePago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPago() throws Exception;

    public List<PagoDTO> getDataPago() throws Exception;

    public void validatePago(Pago pago) throws Exception;
}
