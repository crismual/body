package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IPlanLogic {
    public List<Plan> getPlan() throws Exception;

    /**
         * Save an new Plan entity
         */
    public void savePlan(Plan entity) throws Exception;

    /**
         * Delete an existing Plan entity
         *
         */
    public void deletePlan(Plan entity) throws Exception;

    /**
        * Update an existing Plan entity
        *
        */
    public void updatePlan(Plan entity) throws Exception;

    /**
         * Load an existing Plan entity
         *
         */
    public Plan getPlan(Long plaId) throws Exception;

    public List<Plan> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Plan> findPagePlan(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPlan() throws Exception;

    public List<PlanDTO> getDataPlan() throws Exception;

    public void validatePlan(Plan plan) throws Exception;
}
