package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEjercicioDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ITipoEjercicioLogic {
    public List<TipoEjercicio> getTipoEjercicio() throws Exception;

    /**
         * Save an new TipoEjercicio entity
         */
    public void saveTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    /**
         * Delete an existing TipoEjercicio entity
         *
         */
    public void deleteTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    /**
        * Update an existing TipoEjercicio entity
        *
        */
    public void updateTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    /**
         * Load an existing TipoEjercicio entity
         *
         */
    public TipoEjercicio getTipoEjercicio(Long tiejeId)
        throws Exception;

    public List<TipoEjercicio> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoEjercicio> findPageTipoEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoEjercicio() throws Exception;

    public List<TipoEjercicioDTO> getDataTipoEjercicio()
        throws Exception;

    public void validateTipoEjercicio(TipoEjercicio tipoEjercicio)
        throws Exception;
}
