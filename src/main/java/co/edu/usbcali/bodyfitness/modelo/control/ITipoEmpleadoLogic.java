package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEmpleadoDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ITipoEmpleadoLogic {
    public List<TipoEmpleado> getTipoEmpleado() throws Exception;

    /**
         * Save an new TipoEmpleado entity
         */
    public void saveTipoEmpleado(TipoEmpleado entity) throws Exception;

    /**
         * Delete an existing TipoEmpleado entity
         *
         */
    public void deleteTipoEmpleado(TipoEmpleado entity)
        throws Exception;

    /**
        * Update an existing TipoEmpleado entity
        *
        */
    public void updateTipoEmpleado(TipoEmpleado entity)
        throws Exception;

    /**
         * Load an existing TipoEmpleado entity
         *
         */
    public TipoEmpleado getTipoEmpleado(Long tiemId) throws Exception;

    public List<TipoEmpleado> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoEmpleado> findPageTipoEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoEmpleado() throws Exception;

    public List<TipoEmpleadoDTO> getDataTipoEmpleado()
        throws Exception;

    public void validateTipoEmpleado(TipoEmpleado tipoEmpleado)
        throws Exception;
}
