package co.edu.usbcali.bodyfitness.modelo.control;

import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoPagoDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ITipoPagoLogic {
    public List<TipoPago> getTipoPago() throws Exception;

    /**
         * Save an new TipoPago entity
         */
    public void saveTipoPago(TipoPago entity) throws Exception;

    /**
         * Delete an existing TipoPago entity
         *
         */
    public void deleteTipoPago(TipoPago entity) throws Exception;

    /**
        * Update an existing TipoPago entity
        *
        */
    public void updateTipoPago(TipoPago entity) throws Exception;

    /**
         * Load an existing TipoPago entity
         *
         */
    public TipoPago getTipoPago(Long tipaId) throws Exception;

    public List<TipoPago> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoPago> findPageTipoPago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoPago() throws Exception;

    public List<TipoPagoDTO> getDataTipoPago() throws Exception;

    public void validateTipoPago(TipoPago tipoPago) throws Exception;
}
