package co.edu.usbcali.bodyfitness.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class AntropometriaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AntropometriaDTO.class);
    private Double abdomen;
    private String activo;
    private Long antId;
    private Double antebrazo;
    private Double brazoContraido;
    private Double brazoRelajado;
    private Double cintura;
    private Double cuello;
    private Double estatura;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Double hombro;
    private Double pantorrilla;
    private Double pecho;
    private Double peso;
    private Double pierna;
    private String usuarioCreador;
    private String usuarioModificador;

    public Double getAbdomen() {
        return abdomen;
    }

    public void setAbdomen(Double abdomen) {
        this.abdomen = abdomen;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Long getAntId() {
        return antId;
    }

    public void setAntId(Long antId) {
        this.antId = antId;
    }

    public Double getAntebrazo() {
        return antebrazo;
    }

    public void setAntebrazo(Double antebrazo) {
        this.antebrazo = antebrazo;
    }

    public Double getBrazoContraido() {
        return brazoContraido;
    }

    public void setBrazoContraido(Double brazoContraido) {
        this.brazoContraido = brazoContraido;
    }

    public Double getBrazoRelajado() {
        return brazoRelajado;
    }

    public void setBrazoRelajado(Double brazoRelajado) {
        this.brazoRelajado = brazoRelajado;
    }

    public Double getCintura() {
        return cintura;
    }

    public void setCintura(Double cintura) {
        this.cintura = cintura;
    }

    public Double getCuello() {
        return cuello;
    }

    public void setCuello(Double cuello) {
        this.cuello = cuello;
    }

    public Double getEstatura() {
        return estatura;
    }

    public void setEstatura(Double estatura) {
        this.estatura = estatura;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Double getHombro() {
        return hombro;
    }

    public void setHombro(Double hombro) {
        this.hombro = hombro;
    }

    public Double getPantorrilla() {
        return pantorrilla;
    }

    public void setPantorrilla(Double pantorrilla) {
        this.pantorrilla = pantorrilla;
    }

    public Double getPecho() {
        return pecho;
    }

    public void setPecho(Double pecho) {
        this.pecho = pecho;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getPierna() {
        return pierna;
    }

    public void setPierna(Double pierna) {
        this.pierna = pierna;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}
