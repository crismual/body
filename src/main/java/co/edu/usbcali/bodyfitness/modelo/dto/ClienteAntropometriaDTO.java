package co.edu.usbcali.bodyfitness.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class ClienteAntropometriaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ClienteAntropometriaDTO.class);
    private String activo;
    private Long cliantId;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long antId_Antropometria;
    private Long cliId_Cliente;

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Long getCliantId() {
        return cliantId;
    }

    public void setCliantId(Long cliantId) {
        this.cliantId = cliantId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getAntId_Antropometria() {
        return antId_Antropometria;
    }

    public void setAntId_Antropometria(Long antId_Antropometria) {
        this.antId_Antropometria = antId_Antropometria;
    }

    public Long getCliId_Cliente() {
        return cliId_Cliente;
    }

    public void setCliId_Cliente(Long cliId_Cliente) {
        this.cliId_Cliente = cliId_Cliente;
    }
}
