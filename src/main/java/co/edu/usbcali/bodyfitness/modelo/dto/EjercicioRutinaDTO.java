package co.edu.usbcali.bodyfitness.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class EjercicioRutinaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(EjercicioRutinaDTO.class);
    private String activo;
    private Long ejerutId;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long ejeId_Ejercicio;
    private Long rutId_Rutina;

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Long getEjerutId() {
        return ejerutId;
    }

    public void setEjerutId(Long ejerutId) {
        this.ejerutId = ejerutId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getEjeId_Ejercicio() {
        return ejeId_Ejercicio;
    }

    public void setEjeId_Ejercicio(Long ejeId_Ejercicio) {
        this.ejeId_Ejercicio = ejeId_Ejercicio;
    }

    public Long getRutId_Rutina() {
        return rutId_Rutina;
    }

    public void setRutId_Rutina(Long rutId_Rutina) {
        this.rutId_Rutina = rutId_Rutina;
    }
}
