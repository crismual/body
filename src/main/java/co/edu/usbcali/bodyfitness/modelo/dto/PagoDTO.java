package co.edu.usbcali.bodyfitness.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class PagoDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PagoDTO.class);
    private String activo;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Date fechaPago;
    private Long pagId;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long valor;
    private Long plancliId_PlanCliente;
    private Long tipaId_TipoPago;

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Long getPagId() {
        return pagId;
    }

    public void setPagId(Long pagId) {
        this.pagId = pagId;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public Long getPlancliId_PlanCliente() {
        return plancliId_PlanCliente;
    }

    public void setPlancliId_PlanCliente(Long plancliId_PlanCliente) {
        this.plancliId_PlanCliente = plancliId_PlanCliente;
    }

    public Long getTipaId_TipoPago() {
        return tipaId_TipoPago;
    }

    public void setTipaId_TipoPago(Long tipaId_TipoPago) {
        this.tipaId_TipoPago = tipaId_TipoPago;
    }
}
