package co.edu.usbcali.bodyfitness.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class PlanClienteDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PlanClienteDTO.class);
    private String activo;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Long plancliId;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long cliId_Cliente;
    private Long plaId_Plan;

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getPlancliId() {
        return plancliId;
    }

    public void setPlancliId(Long plancliId) {
        this.plancliId = plancliId;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getCliId_Cliente() {
        return cliId_Cliente;
    }

    public void setCliId_Cliente(Long cliId_Cliente) {
        this.cliId_Cliente = cliId_Cliente;
    }

    public Long getPlaId_Plan() {
        return plaId_Plan;
    }

    public void setPlaId_Plan(Long plaId_Plan) {
        this.plaId_Plan = plaId_Plan;
    }
}
