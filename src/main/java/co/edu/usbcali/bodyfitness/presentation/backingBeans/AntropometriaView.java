package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class AntropometriaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AntropometriaView.class);
    private InputText txtAbdomen;
    private InputText txtActivo;
    private InputText txtAntebrazo;
    private InputText txtBrazoContraido;
    private InputText txtBrazoRelajado;
    private InputText txtCintura;
    private InputText txtCuello;
    private InputText txtEstatura;
    private InputText txtHombro;
    private InputText txtPantorrilla;
    private InputText txtPecho;
    private InputText txtPeso;
    private InputText txtPierna;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtAntId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<AntropometriaDTO> data;
    private AntropometriaDTO selectedAntropometria;
    private Antropometria entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public AntropometriaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedAntropometria = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedAntropometria = null;

        if (txtAbdomen != null) {
            txtAbdomen.setValue(null);
            txtAbdomen.setDisabled(true);
        }

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtAntebrazo != null) {
            txtAntebrazo.setValue(null);
            txtAntebrazo.setDisabled(true);
        }

        if (txtBrazoContraido != null) {
            txtBrazoContraido.setValue(null);
            txtBrazoContraido.setDisabled(true);
        }

        if (txtBrazoRelajado != null) {
            txtBrazoRelajado.setValue(null);
            txtBrazoRelajado.setDisabled(true);
        }

        if (txtCintura != null) {
            txtCintura.setValue(null);
            txtCintura.setDisabled(true);
        }

        if (txtCuello != null) {
            txtCuello.setValue(null);
            txtCuello.setDisabled(true);
        }

        if (txtEstatura != null) {
            txtEstatura.setValue(null);
            txtEstatura.setDisabled(true);
        }

        if (txtHombro != null) {
            txtHombro.setValue(null);
            txtHombro.setDisabled(true);
        }

        if (txtPantorrilla != null) {
            txtPantorrilla.setValue(null);
            txtPantorrilla.setDisabled(true);
        }

        if (txtPecho != null) {
            txtPecho.setValue(null);
            txtPecho.setDisabled(true);
        }

        if (txtPeso != null) {
            txtPeso.setValue(null);
            txtPeso.setDisabled(true);
        }

        if (txtPierna != null) {
            txtPierna.setValue(null);
            txtPierna.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtAntId != null) {
            txtAntId.setValue(null);
            txtAntId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long antId = FacesUtils.checkLong(txtAntId);
            entity = (antId != null)
                ? businessDelegatorView.getAntropometria(antId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtAbdomen.setDisabled(false);
            txtActivo.setDisabled(false);
            txtAntebrazo.setDisabled(false);
            txtBrazoContraido.setDisabled(false);
            txtBrazoRelajado.setDisabled(false);
            txtCintura.setDisabled(false);
            txtCuello.setDisabled(false);
            txtEstatura.setDisabled(false);
            txtHombro.setDisabled(false);
            txtPantorrilla.setDisabled(false);
            txtPecho.setDisabled(false);
            txtPeso.setDisabled(false);
            txtPierna.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtAntId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtAbdomen.setValue(entity.getAbdomen());
            txtAbdomen.setDisabled(false);
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtAntebrazo.setValue(entity.getAntebrazo());
            txtAntebrazo.setDisabled(false);
            txtBrazoContraido.setValue(entity.getBrazoContraido());
            txtBrazoContraido.setDisabled(false);
            txtBrazoRelajado.setValue(entity.getBrazoRelajado());
            txtBrazoRelajado.setDisabled(false);
            txtCintura.setValue(entity.getCintura());
            txtCintura.setDisabled(false);
            txtCuello.setValue(entity.getCuello());
            txtCuello.setDisabled(false);
            txtEstatura.setValue(entity.getEstatura());
            txtEstatura.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtHombro.setValue(entity.getHombro());
            txtHombro.setDisabled(false);
            txtPantorrilla.setValue(entity.getPantorrilla());
            txtPantorrilla.setDisabled(false);
            txtPecho.setValue(entity.getPecho());
            txtPecho.setDisabled(false);
            txtPeso.setValue(entity.getPeso());
            txtPeso.setDisabled(false);
            txtPierna.setValue(entity.getPierna());
            txtPierna.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtAntId.setValue(entity.getAntId());
            txtAntId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedAntropometria = (AntropometriaDTO) (evt.getComponent()
                                                       .getAttributes()
                                                       .get("selectedAntropometria"));
        txtAbdomen.setValue(selectedAntropometria.getAbdomen());
        txtAbdomen.setDisabled(false);
        txtActivo.setValue(selectedAntropometria.getActivo());
        txtActivo.setDisabled(false);
        txtAntebrazo.setValue(selectedAntropometria.getAntebrazo());
        txtAntebrazo.setDisabled(false);
        txtBrazoContraido.setValue(selectedAntropometria.getBrazoContraido());
        txtBrazoContraido.setDisabled(false);
        txtBrazoRelajado.setValue(selectedAntropometria.getBrazoRelajado());
        txtBrazoRelajado.setDisabled(false);
        txtCintura.setValue(selectedAntropometria.getCintura());
        txtCintura.setDisabled(false);
        txtCuello.setValue(selectedAntropometria.getCuello());
        txtCuello.setDisabled(false);
        txtEstatura.setValue(selectedAntropometria.getEstatura());
        txtEstatura.setDisabled(false);
        txtFechaCreacion.setValue(selectedAntropometria.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedAntropometria.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtHombro.setValue(selectedAntropometria.getHombro());
        txtHombro.setDisabled(false);
        txtPantorrilla.setValue(selectedAntropometria.getPantorrilla());
        txtPantorrilla.setDisabled(false);
        txtPecho.setValue(selectedAntropometria.getPecho());
        txtPecho.setDisabled(false);
        txtPeso.setValue(selectedAntropometria.getPeso());
        txtPeso.setDisabled(false);
        txtPierna.setValue(selectedAntropometria.getPierna());
        txtPierna.setDisabled(false);
        txtUsuarioCreador.setValue(selectedAntropometria.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedAntropometria.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtAntId.setValue(selectedAntropometria.getAntId());
        txtAntId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedAntropometria == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Antropometria();

            Long antId = FacesUtils.checkLong(txtAntId);

            entity.setAbdomen(FacesUtils.checkDouble(txtAbdomen));
            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setAntId(antId);
            entity.setAntebrazo(FacesUtils.checkDouble(txtAntebrazo));
            entity.setBrazoContraido(FacesUtils.checkDouble(txtBrazoContraido));
            entity.setBrazoRelajado(FacesUtils.checkDouble(txtBrazoRelajado));
            entity.setCintura(FacesUtils.checkDouble(txtCintura));
            entity.setCuello(FacesUtils.checkDouble(txtCuello));
            entity.setEstatura(FacesUtils.checkDouble(txtEstatura));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setHombro(FacesUtils.checkDouble(txtHombro));
            entity.setPantorrilla(FacesUtils.checkDouble(txtPantorrilla));
            entity.setPecho(FacesUtils.checkDouble(txtPecho));
            entity.setPeso(FacesUtils.checkDouble(txtPeso));
            entity.setPierna(FacesUtils.checkDouble(txtPierna));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            businessDelegatorView.saveAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long antId = new Long(selectedAntropometria.getAntId());
                entity = businessDelegatorView.getAntropometria(antId);
            }

            entity.setAbdomen(FacesUtils.checkDouble(txtAbdomen));
            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setAntebrazo(FacesUtils.checkDouble(txtAntebrazo));
            entity.setBrazoContraido(FacesUtils.checkDouble(txtBrazoContraido));
            entity.setBrazoRelajado(FacesUtils.checkDouble(txtBrazoRelajado));
            entity.setCintura(FacesUtils.checkDouble(txtCintura));
            entity.setCuello(FacesUtils.checkDouble(txtCuello));
            entity.setEstatura(FacesUtils.checkDouble(txtEstatura));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setHombro(FacesUtils.checkDouble(txtHombro));
            entity.setPantorrilla(FacesUtils.checkDouble(txtPantorrilla));
            entity.setPecho(FacesUtils.checkDouble(txtPecho));
            entity.setPeso(FacesUtils.checkDouble(txtPeso));
            entity.setPierna(FacesUtils.checkDouble(txtPierna));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            businessDelegatorView.updateAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedAntropometria = (AntropometriaDTO) (evt.getComponent()
                                                           .getAttributes()
                                                           .get("selectedAntropometria"));

            Long antId = new Long(selectedAntropometria.getAntId());
            entity = businessDelegatorView.getAntropometria(antId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long antId = FacesUtils.checkLong(txtAntId);
            entity = businessDelegatorView.getAntropometria(antId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Double abdomen, String activo,
        Long antId, Double antebrazo, Double brazoContraido,
        Double brazoRelajado, Double cintura, Double cuello, Double estatura,
        Date fechaCreacion, Date fechaModificacion, Double hombro,
        Double pantorrilla, Double pecho, Double peso, Double pierna,
        String usuarioCreador, String usuarioModificador)
        throws Exception {
        try {
            entity.setAbdomen(FacesUtils.checkDouble(abdomen));
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setAntebrazo(FacesUtils.checkDouble(antebrazo));
            entity.setBrazoContraido(FacesUtils.checkDouble(brazoContraido));
            entity.setBrazoRelajado(FacesUtils.checkDouble(brazoRelajado));
            entity.setCintura(FacesUtils.checkDouble(cintura));
            entity.setCuello(FacesUtils.checkDouble(cuello));
            entity.setEstatura(FacesUtils.checkDouble(estatura));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setHombro(FacesUtils.checkDouble(hombro));
            entity.setPantorrilla(FacesUtils.checkDouble(pantorrilla));
            entity.setPecho(FacesUtils.checkDouble(pecho));
            entity.setPeso(FacesUtils.checkDouble(peso));
            entity.setPierna(FacesUtils.checkDouble(pierna));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("AntropometriaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtAbdomen() {
        return txtAbdomen;
    }

    public void setTxtAbdomen(InputText txtAbdomen) {
        this.txtAbdomen = txtAbdomen;
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtAntebrazo() {
        return txtAntebrazo;
    }

    public void setTxtAntebrazo(InputText txtAntebrazo) {
        this.txtAntebrazo = txtAntebrazo;
    }

    public InputText getTxtBrazoContraido() {
        return txtBrazoContraido;
    }

    public void setTxtBrazoContraido(InputText txtBrazoContraido) {
        this.txtBrazoContraido = txtBrazoContraido;
    }

    public InputText getTxtBrazoRelajado() {
        return txtBrazoRelajado;
    }

    public void setTxtBrazoRelajado(InputText txtBrazoRelajado) {
        this.txtBrazoRelajado = txtBrazoRelajado;
    }

    public InputText getTxtCintura() {
        return txtCintura;
    }

    public void setTxtCintura(InputText txtCintura) {
        this.txtCintura = txtCintura;
    }

    public InputText getTxtCuello() {
        return txtCuello;
    }

    public void setTxtCuello(InputText txtCuello) {
        this.txtCuello = txtCuello;
    }

    public InputText getTxtEstatura() {
        return txtEstatura;
    }

    public void setTxtEstatura(InputText txtEstatura) {
        this.txtEstatura = txtEstatura;
    }

    public InputText getTxtHombro() {
        return txtHombro;
    }

    public void setTxtHombro(InputText txtHombro) {
        this.txtHombro = txtHombro;
    }

    public InputText getTxtPantorrilla() {
        return txtPantorrilla;
    }

    public void setTxtPantorrilla(InputText txtPantorrilla) {
        this.txtPantorrilla = txtPantorrilla;
    }

    public InputText getTxtPecho() {
        return txtPecho;
    }

    public void setTxtPecho(InputText txtPecho) {
        this.txtPecho = txtPecho;
    }

    public InputText getTxtPeso() {
        return txtPeso;
    }

    public void setTxtPeso(InputText txtPeso) {
        this.txtPeso = txtPeso;
    }

    public InputText getTxtPierna() {
        return txtPierna;
    }

    public void setTxtPierna(InputText txtPierna) {
        this.txtPierna = txtPierna;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtAntId() {
        return txtAntId;
    }

    public void setTxtAntId(InputText txtAntId) {
        this.txtAntId = txtAntId;
    }

    public List<AntropometriaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataAntropometria();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<AntropometriaDTO> antropometriaDTO) {
        this.data = antropometriaDTO;
    }

    public AntropometriaDTO getSelectedAntropometria() {
        return selectedAntropometria;
    }

    public void setSelectedAntropometria(AntropometriaDTO antropometria) {
        this.selectedAntropometria = antropometria;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
