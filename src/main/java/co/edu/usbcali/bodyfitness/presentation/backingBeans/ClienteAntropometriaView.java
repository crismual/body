package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ClienteAntropometriaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ClienteAntropometriaView.class);
    private InputText txtActivo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtAntId_Antropometria;
    private InputText txtCliId_Cliente;
    private InputText txtCliantId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<ClienteAntropometriaDTO> data;
    private ClienteAntropometriaDTO selectedClienteAntropometria;
    private ClienteAntropometria entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ClienteAntropometriaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedClienteAntropometria = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedClienteAntropometria = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtAntId_Antropometria != null) {
            txtAntId_Antropometria.setValue(null);
            txtAntId_Antropometria.setDisabled(true);
        }

        if (txtCliId_Cliente != null) {
            txtCliId_Cliente.setValue(null);
            txtCliId_Cliente.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtCliantId != null) {
            txtCliantId.setValue(null);
            txtCliantId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long cliantId = FacesUtils.checkLong(txtCliantId);
            entity = (cliantId != null)
                ? businessDelegatorView.getClienteAntropometria(cliantId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtAntId_Antropometria.setDisabled(false);
            txtCliId_Cliente.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtCliantId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtAntId_Antropometria.setValue(entity.getAntropometria().getAntId());
            txtAntId_Antropometria.setDisabled(false);
            txtCliId_Cliente.setValue(entity.getCliente().getCliId());
            txtCliId_Cliente.setDisabled(false);
            txtCliantId.setValue(entity.getCliantId());
            txtCliantId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedClienteAntropometria = (ClienteAntropometriaDTO) (evt.getComponent()
                                                                     .getAttributes()
                                                                     .get("selectedClienteAntropometria"));
        txtActivo.setValue(selectedClienteAntropometria.getActivo());
        txtActivo.setDisabled(false);
        txtFechaCreacion.setValue(selectedClienteAntropometria.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedClienteAntropometria.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedClienteAntropometria.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedClienteAntropometria.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtAntId_Antropometria.setValue(selectedClienteAntropometria.getAntId_Antropometria());
        txtAntId_Antropometria.setDisabled(false);
        txtCliId_Cliente.setValue(selectedClienteAntropometria.getCliId_Cliente());
        txtCliId_Cliente.setDisabled(false);
        txtCliantId.setValue(selectedClienteAntropometria.getCliantId());
        txtCliantId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedClienteAntropometria == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new ClienteAntropometria();

            Long cliantId = FacesUtils.checkLong(txtCliantId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setCliantId(cliantId);
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setAntropometria((FacesUtils.checkLong(
                    txtAntId_Antropometria) != null)
                ? businessDelegatorView.getAntropometria(FacesUtils.checkLong(
                        txtAntId_Antropometria)) : null);
            entity.setCliente((FacesUtils.checkLong(txtCliId_Cliente) != null)
                ? businessDelegatorView.getCliente(FacesUtils.checkLong(
                        txtCliId_Cliente)) : null);
            businessDelegatorView.saveClienteAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long cliantId = new Long(selectedClienteAntropometria.getCliantId());
                entity = businessDelegatorView.getClienteAntropometria(cliantId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setAntropometria((FacesUtils.checkLong(
                    txtAntId_Antropometria) != null)
                ? businessDelegatorView.getAntropometria(FacesUtils.checkLong(
                        txtAntId_Antropometria)) : null);
            entity.setCliente((FacesUtils.checkLong(txtCliId_Cliente) != null)
                ? businessDelegatorView.getCliente(FacesUtils.checkLong(
                        txtCliId_Cliente)) : null);
            businessDelegatorView.updateClienteAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedClienteAntropometria = (ClienteAntropometriaDTO) (evt.getComponent()
                                                                         .getAttributes()
                                                                         .get("selectedClienteAntropometria"));

            Long cliantId = new Long(selectedClienteAntropometria.getCliantId());
            entity = businessDelegatorView.getClienteAntropometria(cliantId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long cliantId = FacesUtils.checkLong(txtCliantId);
            entity = businessDelegatorView.getClienteAntropometria(cliantId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteClienteAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Long cliantId,
        Date fechaCreacion, Date fechaModificacion, String usuarioCreador,
        String usuarioModificador, Long antId_Antropometria, Long cliId_Cliente)
        throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateClienteAntropometria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ClienteAntropometriaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtAntId_Antropometria() {
        return txtAntId_Antropometria;
    }

    public void setTxtAntId_Antropometria(InputText txtAntId_Antropometria) {
        this.txtAntId_Antropometria = txtAntId_Antropometria;
    }

    public InputText getTxtCliId_Cliente() {
        return txtCliId_Cliente;
    }

    public void setTxtCliId_Cliente(InputText txtCliId_Cliente) {
        this.txtCliId_Cliente = txtCliId_Cliente;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtCliantId() {
        return txtCliantId;
    }

    public void setTxtCliantId(InputText txtCliantId) {
        this.txtCliantId = txtCliantId;
    }

    public List<ClienteAntropometriaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataClienteAntropometria();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ClienteAntropometriaDTO> clienteAntropometriaDTO) {
        this.data = clienteAntropometriaDTO;
    }

    public ClienteAntropometriaDTO getSelectedClienteAntropometria() {
        return selectedClienteAntropometria;
    }

    public void setSelectedClienteAntropometria(
        ClienteAntropometriaDTO clienteAntropometria) {
        this.selectedClienteAntropometria = clienteAntropometria;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
