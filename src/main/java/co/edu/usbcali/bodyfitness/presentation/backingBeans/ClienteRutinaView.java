package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ClienteRutinaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ClienteRutinaView.class);
    private InputText txtActivo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtCliId_Cliente;
    private InputText txtRutId_Rutina;
    private InputText txtClirutId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<ClienteRutinaDTO> data;
    private ClienteRutinaDTO selectedClienteRutina;
    private ClienteRutina entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ClienteRutinaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedClienteRutina = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedClienteRutina = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtCliId_Cliente != null) {
            txtCliId_Cliente.setValue(null);
            txtCliId_Cliente.setDisabled(true);
        }

        if (txtRutId_Rutina != null) {
            txtRutId_Rutina.setValue(null);
            txtRutId_Rutina.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtClirutId != null) {
            txtClirutId.setValue(null);
            txtClirutId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long clirutId = FacesUtils.checkLong(txtClirutId);
            entity = (clirutId != null)
                ? businessDelegatorView.getClienteRutina(clirutId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtCliId_Cliente.setDisabled(false);
            txtRutId_Rutina.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtClirutId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtCliId_Cliente.setValue(entity.getCliente().getCliId());
            txtCliId_Cliente.setDisabled(false);
            txtRutId_Rutina.setValue(entity.getRutina().getRutId());
            txtRutId_Rutina.setDisabled(false);
            txtClirutId.setValue(entity.getClirutId());
            txtClirutId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedClienteRutina = (ClienteRutinaDTO) (evt.getComponent()
                                                       .getAttributes()
                                                       .get("selectedClienteRutina"));
        txtActivo.setValue(selectedClienteRutina.getActivo());
        txtActivo.setDisabled(false);
        txtFechaCreacion.setValue(selectedClienteRutina.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedClienteRutina.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedClienteRutina.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedClienteRutina.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtCliId_Cliente.setValue(selectedClienteRutina.getCliId_Cliente());
        txtCliId_Cliente.setDisabled(false);
        txtRutId_Rutina.setValue(selectedClienteRutina.getRutId_Rutina());
        txtRutId_Rutina.setDisabled(false);
        txtClirutId.setValue(selectedClienteRutina.getClirutId());
        txtClirutId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedClienteRutina == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new ClienteRutina();

            Long clirutId = FacesUtils.checkLong(txtClirutId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setClirutId(clirutId);
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setCliente((FacesUtils.checkLong(txtCliId_Cliente) != null)
                ? businessDelegatorView.getCliente(FacesUtils.checkLong(
                        txtCliId_Cliente)) : null);
            entity.setRutina((FacesUtils.checkLong(txtRutId_Rutina) != null)
                ? businessDelegatorView.getRutina(FacesUtils.checkLong(
                        txtRutId_Rutina)) : null);
            businessDelegatorView.saveClienteRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long clirutId = new Long(selectedClienteRutina.getClirutId());
                entity = businessDelegatorView.getClienteRutina(clirutId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setCliente((FacesUtils.checkLong(txtCliId_Cliente) != null)
                ? businessDelegatorView.getCliente(FacesUtils.checkLong(
                        txtCliId_Cliente)) : null);
            entity.setRutina((FacesUtils.checkLong(txtRutId_Rutina) != null)
                ? businessDelegatorView.getRutina(FacesUtils.checkLong(
                        txtRutId_Rutina)) : null);
            businessDelegatorView.updateClienteRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedClienteRutina = (ClienteRutinaDTO) (evt.getComponent()
                                                           .getAttributes()
                                                           .get("selectedClienteRutina"));

            Long clirutId = new Long(selectedClienteRutina.getClirutId());
            entity = businessDelegatorView.getClienteRutina(clirutId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long clirutId = FacesUtils.checkLong(txtClirutId);
            entity = businessDelegatorView.getClienteRutina(clirutId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteClienteRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Long clirutId,
        Date fechaCreacion, Date fechaModificacion, String usuarioCreador,
        String usuarioModificador, Long cliId_Cliente, Long rutId_Rutina)
        throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateClienteRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ClienteRutinaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtCliId_Cliente() {
        return txtCliId_Cliente;
    }

    public void setTxtCliId_Cliente(InputText txtCliId_Cliente) {
        this.txtCliId_Cliente = txtCliId_Cliente;
    }

    public InputText getTxtRutId_Rutina() {
        return txtRutId_Rutina;
    }

    public void setTxtRutId_Rutina(InputText txtRutId_Rutina) {
        this.txtRutId_Rutina = txtRutId_Rutina;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtClirutId() {
        return txtClirutId;
    }

    public void setTxtClirutId(InputText txtClirutId) {
        this.txtClirutId = txtClirutId;
    }

    public List<ClienteRutinaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataClienteRutina();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ClienteRutinaDTO> clienteRutinaDTO) {
        this.data = clienteRutinaDTO;
    }

    public ClienteRutinaDTO getSelectedClienteRutina() {
        return selectedClienteRutina;
    }

    public void setSelectedClienteRutina(ClienteRutinaDTO clienteRutina) {
        this.selectedClienteRutina = clienteRutina;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
