package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class EjercicioRutinaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(EjercicioRutinaView.class);
    private InputText txtActivo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtEjeId_Ejercicio;
    private InputText txtRutId_Rutina;
    private InputText txtEjerutId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<EjercicioRutinaDTO> data;
    private EjercicioRutinaDTO selectedEjercicioRutina;
    private EjercicioRutina entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public EjercicioRutinaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedEjercicioRutina = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedEjercicioRutina = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtEjeId_Ejercicio != null) {
            txtEjeId_Ejercicio.setValue(null);
            txtEjeId_Ejercicio.setDisabled(true);
        }

        if (txtRutId_Rutina != null) {
            txtRutId_Rutina.setValue(null);
            txtRutId_Rutina.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtEjerutId != null) {
            txtEjerutId.setValue(null);
            txtEjerutId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long ejerutId = FacesUtils.checkLong(txtEjerutId);
            entity = (ejerutId != null)
                ? businessDelegatorView.getEjercicioRutina(ejerutId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtEjeId_Ejercicio.setDisabled(false);
            txtRutId_Rutina.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtEjerutId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtEjeId_Ejercicio.setValue(entity.getEjercicio().getEjeId());
            txtEjeId_Ejercicio.setDisabled(false);
            txtRutId_Rutina.setValue(entity.getRutina().getRutId());
            txtRutId_Rutina.setDisabled(false);
            txtEjerutId.setValue(entity.getEjerutId());
            txtEjerutId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedEjercicioRutina = (EjercicioRutinaDTO) (evt.getComponent()
                                                           .getAttributes()
                                                           .get("selectedEjercicioRutina"));
        txtActivo.setValue(selectedEjercicioRutina.getActivo());
        txtActivo.setDisabled(false);
        txtFechaCreacion.setValue(selectedEjercicioRutina.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedEjercicioRutina.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedEjercicioRutina.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedEjercicioRutina.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtEjeId_Ejercicio.setValue(selectedEjercicioRutina.getEjeId_Ejercicio());
        txtEjeId_Ejercicio.setDisabled(false);
        txtRutId_Rutina.setValue(selectedEjercicioRutina.getRutId_Rutina());
        txtRutId_Rutina.setDisabled(false);
        txtEjerutId.setValue(selectedEjercicioRutina.getEjerutId());
        txtEjerutId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedEjercicioRutina == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new EjercicioRutina();

            Long ejerutId = FacesUtils.checkLong(txtEjerutId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setEjerutId(ejerutId);
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setEjercicio((FacesUtils.checkLong(txtEjeId_Ejercicio) != null)
                ? businessDelegatorView.getEjercicio(FacesUtils.checkLong(
                        txtEjeId_Ejercicio)) : null);
            entity.setRutina((FacesUtils.checkLong(txtRutId_Rutina) != null)
                ? businessDelegatorView.getRutina(FacesUtils.checkLong(
                        txtRutId_Rutina)) : null);
            businessDelegatorView.saveEjercicioRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long ejerutId = new Long(selectedEjercicioRutina.getEjerutId());
                entity = businessDelegatorView.getEjercicioRutina(ejerutId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setEjercicio((FacesUtils.checkLong(txtEjeId_Ejercicio) != null)
                ? businessDelegatorView.getEjercicio(FacesUtils.checkLong(
                        txtEjeId_Ejercicio)) : null);
            entity.setRutina((FacesUtils.checkLong(txtRutId_Rutina) != null)
                ? businessDelegatorView.getRutina(FacesUtils.checkLong(
                        txtRutId_Rutina)) : null);
            businessDelegatorView.updateEjercicioRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedEjercicioRutina = (EjercicioRutinaDTO) (evt.getComponent()
                                                               .getAttributes()
                                                               .get("selectedEjercicioRutina"));

            Long ejerutId = new Long(selectedEjercicioRutina.getEjerutId());
            entity = businessDelegatorView.getEjercicioRutina(ejerutId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long ejerutId = FacesUtils.checkLong(txtEjerutId);
            entity = businessDelegatorView.getEjercicioRutina(ejerutId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteEjercicioRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Long ejerutId,
        Date fechaCreacion, Date fechaModificacion, String usuarioCreador,
        String usuarioModificador, Long ejeId_Ejercicio, Long rutId_Rutina)
        throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateEjercicioRutina(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("EjercicioRutinaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtEjeId_Ejercicio() {
        return txtEjeId_Ejercicio;
    }

    public void setTxtEjeId_Ejercicio(InputText txtEjeId_Ejercicio) {
        this.txtEjeId_Ejercicio = txtEjeId_Ejercicio;
    }

    public InputText getTxtRutId_Rutina() {
        return txtRutId_Rutina;
    }

    public void setTxtRutId_Rutina(InputText txtRutId_Rutina) {
        this.txtRutId_Rutina = txtRutId_Rutina;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtEjerutId() {
        return txtEjerutId;
    }

    public void setTxtEjerutId(InputText txtEjerutId) {
        this.txtEjerutId = txtEjerutId;
    }

    public List<EjercicioRutinaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataEjercicioRutina();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<EjercicioRutinaDTO> ejercicioRutinaDTO) {
        this.data = ejercicioRutinaDTO;
    }

    public EjercicioRutinaDTO getSelectedEjercicioRutina() {
        return selectedEjercicioRutina;
    }

    public void setSelectedEjercicioRutina(EjercicioRutinaDTO ejercicioRutina) {
        this.selectedEjercicioRutina = ejercicioRutina;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
