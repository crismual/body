package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class EjercicioView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(EjercicioView.class);
    private InputText txtActivo;
    private InputText txtDescripcion;
    private InputText txtDia;
    private InputText txtImagen;
    private InputText txtNombre;
    private InputText txtRepeticiones;
    private InputText txtSeries;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtTiejeId_TipoEjercicio;
    private InputText txtEjeId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<EjercicioDTO> data;
    private EjercicioDTO selectedEjercicio;
    private Ejercicio entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public EjercicioView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedEjercicio = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedEjercicio = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtDia != null) {
            txtDia.setValue(null);
            txtDia.setDisabled(true);
        }

        if (txtImagen != null) {
            txtImagen.setValue(null);
            txtImagen.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtRepeticiones != null) {
            txtRepeticiones.setValue(null);
            txtRepeticiones.setDisabled(true);
        }

        if (txtSeries != null) {
            txtSeries.setValue(null);
            txtSeries.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtTiejeId_TipoEjercicio != null) {
            txtTiejeId_TipoEjercicio.setValue(null);
            txtTiejeId_TipoEjercicio.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtEjeId != null) {
            txtEjeId.setValue(null);
            txtEjeId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long ejeId = FacesUtils.checkLong(txtEjeId);
            entity = (ejeId != null)
                ? businessDelegatorView.getEjercicio(ejeId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtDia.setDisabled(false);
            txtImagen.setDisabled(false);
            txtNombre.setDisabled(false);
            txtRepeticiones.setDisabled(false);
            txtSeries.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtTiejeId_TipoEjercicio.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtEjeId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtDia.setValue(entity.getDia());
            txtDia.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtImagen.setValue(entity.getImagen());
            txtImagen.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtRepeticiones.setValue(entity.getRepeticiones());
            txtRepeticiones.setDisabled(false);
            txtSeries.setValue(entity.getSeries());
            txtSeries.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtTiejeId_TipoEjercicio.setValue(entity.getTipoEjercicio()
                                                    .getTiejeId());
            txtTiejeId_TipoEjercicio.setDisabled(false);
            txtEjeId.setValue(entity.getEjeId());
            txtEjeId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedEjercicio = (EjercicioDTO) (evt.getComponent().getAttributes()
                                               .get("selectedEjercicio"));
        txtActivo.setValue(selectedEjercicio.getActivo());
        txtActivo.setDisabled(false);
        txtDescripcion.setValue(selectedEjercicio.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtDia.setValue(selectedEjercicio.getDia());
        txtDia.setDisabled(false);
        txtFechaCreacion.setValue(selectedEjercicio.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedEjercicio.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtImagen.setValue(selectedEjercicio.getImagen());
        txtImagen.setDisabled(false);
        txtNombre.setValue(selectedEjercicio.getNombre());
        txtNombre.setDisabled(false);
        txtRepeticiones.setValue(selectedEjercicio.getRepeticiones());
        txtRepeticiones.setDisabled(false);
        txtSeries.setValue(selectedEjercicio.getSeries());
        txtSeries.setDisabled(false);
        txtUsuarioCreador.setValue(selectedEjercicio.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedEjercicio.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtTiejeId_TipoEjercicio.setValue(selectedEjercicio.getTiejeId_TipoEjercicio());
        txtTiejeId_TipoEjercicio.setDisabled(false);
        txtEjeId.setValue(selectedEjercicio.getEjeId());
        txtEjeId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedEjercicio == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Ejercicio();

            Long ejeId = FacesUtils.checkLong(txtEjeId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDia(FacesUtils.checkString(txtDia));
            entity.setEjeId(ejeId);
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setImagen(FacesUtils.checkString(txtImagen));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setRepeticiones(FacesUtils.checkLong(txtRepeticiones));
            entity.setSeries(FacesUtils.checkLong(txtSeries));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoEjercicio((FacesUtils.checkLong(
                    txtTiejeId_TipoEjercicio) != null)
                ? businessDelegatorView.getTipoEjercicio(FacesUtils.checkLong(
                        txtTiejeId_TipoEjercicio)) : null);
            businessDelegatorView.saveEjercicio(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long ejeId = new Long(selectedEjercicio.getEjeId());
                entity = businessDelegatorView.getEjercicio(ejeId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDia(FacesUtils.checkString(txtDia));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setImagen(FacesUtils.checkString(txtImagen));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setRepeticiones(FacesUtils.checkLong(txtRepeticiones));
            entity.setSeries(FacesUtils.checkLong(txtSeries));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoEjercicio((FacesUtils.checkLong(
                    txtTiejeId_TipoEjercicio) != null)
                ? businessDelegatorView.getTipoEjercicio(FacesUtils.checkLong(
                        txtTiejeId_TipoEjercicio)) : null);
            businessDelegatorView.updateEjercicio(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedEjercicio = (EjercicioDTO) (evt.getComponent()
                                                   .getAttributes()
                                                   .get("selectedEjercicio"));

            Long ejeId = new Long(selectedEjercicio.getEjeId());
            entity = businessDelegatorView.getEjercicio(ejeId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long ejeId = FacesUtils.checkLong(txtEjeId);
            entity = businessDelegatorView.getEjercicio(ejeId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteEjercicio(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, String descripcion,
        String dia, Long ejeId, Date fechaCreacion, Date fechaModificacion,
        String imagen, String nombre, Long repeticiones, Long series,
        String usuarioCreador, String usuarioModificador,
        Long tiejeId_TipoEjercicio) throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setDia(FacesUtils.checkString(dia));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setImagen(FacesUtils.checkString(imagen));
            entity.setNombre(FacesUtils.checkString(nombre));
            entity.setRepeticiones(FacesUtils.checkLong(repeticiones));
            entity.setSeries(FacesUtils.checkLong(series));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateEjercicio(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("EjercicioView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtDia() {
        return txtDia;
    }

    public void setTxtDia(InputText txtDia) {
        this.txtDia = txtDia;
    }

    public InputText getTxtImagen() {
        return txtImagen;
    }

    public void setTxtImagen(InputText txtImagen) {
        this.txtImagen = txtImagen;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtRepeticiones() {
        return txtRepeticiones;
    }

    public void setTxtRepeticiones(InputText txtRepeticiones) {
        this.txtRepeticiones = txtRepeticiones;
    }

    public InputText getTxtSeries() {
        return txtSeries;
    }

    public void setTxtSeries(InputText txtSeries) {
        this.txtSeries = txtSeries;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtTiejeId_TipoEjercicio() {
        return txtTiejeId_TipoEjercicio;
    }

    public void setTxtTiejeId_TipoEjercicio(InputText txtTiejeId_TipoEjercicio) {
        this.txtTiejeId_TipoEjercicio = txtTiejeId_TipoEjercicio;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtEjeId() {
        return txtEjeId;
    }

    public void setTxtEjeId(InputText txtEjeId) {
        this.txtEjeId = txtEjeId;
    }

    public List<EjercicioDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataEjercicio();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<EjercicioDTO> ejercicioDTO) {
        this.data = ejercicioDTO;
    }

    public EjercicioDTO getSelectedEjercicio() {
        return selectedEjercicio;
    }

    public void setSelectedEjercicio(EjercicioDTO ejercicio) {
        this.selectedEjercicio = ejercicio;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
