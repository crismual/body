package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class EmpleadoView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(EmpleadoView.class);
    private InputText txtActivo;
    private InputText txtApellido;
    private InputText txtCedula;
    private InputText txtCelular;
    private InputText txtCorreo;
    private InputText txtDireccion;
    private InputText txtFoto;
    private InputText txtNombre;
    private InputText txtPassword;
    private InputText txtSexo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtTiemId_TipoEmpleado;
    private InputText txtEmpId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private Calendar txtFechaNacimiento;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<EmpleadoDTO> data;
    private EmpleadoDTO selectedEmpleado;
    private Empleado entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public EmpleadoView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedEmpleado = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedEmpleado = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtApellido != null) {
            txtApellido.setValue(null);
            txtApellido.setDisabled(true);
        }

        if (txtCedula != null) {
            txtCedula.setValue(null);
            txtCedula.setDisabled(true);
        }

        if (txtCelular != null) {
            txtCelular.setValue(null);
            txtCelular.setDisabled(true);
        }

        if (txtCorreo != null) {
            txtCorreo.setValue(null);
            txtCorreo.setDisabled(true);
        }

        if (txtDireccion != null) {
            txtDireccion.setValue(null);
            txtDireccion.setDisabled(true);
        }

        if (txtFoto != null) {
            txtFoto.setValue(null);
            txtFoto.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtPassword != null) {
            txtPassword.setValue(null);
            txtPassword.setDisabled(true);
        }

        if (txtSexo != null) {
            txtSexo.setValue(null);
            txtSexo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtTiemId_TipoEmpleado != null) {
            txtTiemId_TipoEmpleado.setValue(null);
            txtTiemId_TipoEmpleado.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtFechaNacimiento != null) {
            txtFechaNacimiento.setValue(null);
            txtFechaNacimiento.setDisabled(true);
        }

        if (txtEmpId != null) {
            txtEmpId.setValue(null);
            txtEmpId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaNacimiento() {
        Date inputDate = (Date) txtFechaNacimiento.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long empId = FacesUtils.checkLong(txtEmpId);
            entity = (empId != null) ? businessDelegatorView.getEmpleado(empId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtApellido.setDisabled(false);
            txtCedula.setDisabled(false);
            txtCelular.setDisabled(false);
            txtCorreo.setDisabled(false);
            txtDireccion.setDisabled(false);
            txtFoto.setDisabled(false);
            txtNombre.setDisabled(false);
            txtPassword.setDisabled(false);
            txtSexo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtTiemId_TipoEmpleado.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtFechaNacimiento.setDisabled(false);
            txtEmpId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtApellido.setValue(entity.getApellido());
            txtApellido.setDisabled(false);
            txtCedula.setValue(entity.getCedula());
            txtCedula.setDisabled(false);
            txtCelular.setValue(entity.getCelular());
            txtCelular.setDisabled(false);
            txtCorreo.setValue(entity.getCorreo());
            txtCorreo.setDisabled(false);
            txtDireccion.setValue(entity.getDireccion());
            txtDireccion.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtFechaNacimiento.setValue(entity.getFechaNacimiento());
            txtFechaNacimiento.setDisabled(false);
            txtFoto.setValue(entity.getFoto());
            txtFoto.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtPassword.setValue(entity.getPassword());
            txtPassword.setDisabled(false);
            txtSexo.setValue(entity.getSexo());
            txtSexo.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtTiemId_TipoEmpleado.setValue(entity.getTipoEmpleado().getTiemId());
            txtTiemId_TipoEmpleado.setDisabled(false);
            txtEmpId.setValue(entity.getEmpId());
            txtEmpId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedEmpleado = (EmpleadoDTO) (evt.getComponent().getAttributes()
                                             .get("selectedEmpleado"));
        txtActivo.setValue(selectedEmpleado.getActivo());
        txtActivo.setDisabled(false);
        txtApellido.setValue(selectedEmpleado.getApellido());
        txtApellido.setDisabled(false);
        txtCedula.setValue(selectedEmpleado.getCedula());
        txtCedula.setDisabled(false);
        txtCelular.setValue(selectedEmpleado.getCelular());
        txtCelular.setDisabled(false);
        txtCorreo.setValue(selectedEmpleado.getCorreo());
        txtCorreo.setDisabled(false);
        txtDireccion.setValue(selectedEmpleado.getDireccion());
        txtDireccion.setDisabled(false);
        txtFechaCreacion.setValue(selectedEmpleado.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedEmpleado.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtFechaNacimiento.setValue(selectedEmpleado.getFechaNacimiento());
        txtFechaNacimiento.setDisabled(false);
        txtFoto.setValue(selectedEmpleado.getFoto());
        txtFoto.setDisabled(false);
        txtNombre.setValue(selectedEmpleado.getNombre());
        txtNombre.setDisabled(false);
        txtPassword.setValue(selectedEmpleado.getPassword());
        txtPassword.setDisabled(false);
        txtSexo.setValue(selectedEmpleado.getSexo());
        txtSexo.setDisabled(false);
        txtUsuarioCreador.setValue(selectedEmpleado.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedEmpleado.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtTiemId_TipoEmpleado.setValue(selectedEmpleado.getTiemId_TipoEmpleado());
        txtTiemId_TipoEmpleado.setDisabled(false);
        txtEmpId.setValue(selectedEmpleado.getEmpId());
        txtEmpId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedEmpleado == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Empleado();

            Long empId = FacesUtils.checkLong(txtEmpId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCelular(FacesUtils.checkLong(txtCelular));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setDireccion(FacesUtils.checkString(txtDireccion));
            entity.setEmpId(empId);
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setFoto(FacesUtils.checkString(txtFoto));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setPassword(FacesUtils.checkString(txtPassword));
            entity.setSexo(FacesUtils.checkString(txtSexo));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoEmpleado((FacesUtils.checkLong(txtTiemId_TipoEmpleado) != null)
                ? businessDelegatorView.getTipoEmpleado(FacesUtils.checkLong(
                        txtTiemId_TipoEmpleado)) : null);
            businessDelegatorView.saveEmpleado(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long empId = new Long(selectedEmpleado.getEmpId());
                entity = businessDelegatorView.getEmpleado(empId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCelular(FacesUtils.checkLong(txtCelular));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setDireccion(FacesUtils.checkString(txtDireccion));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setFoto(FacesUtils.checkString(txtFoto));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setPassword(FacesUtils.checkString(txtPassword));
            entity.setSexo(FacesUtils.checkString(txtSexo));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoEmpleado((FacesUtils.checkLong(txtTiemId_TipoEmpleado) != null)
                ? businessDelegatorView.getTipoEmpleado(FacesUtils.checkLong(
                        txtTiemId_TipoEmpleado)) : null);
            businessDelegatorView.updateEmpleado(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedEmpleado = (EmpleadoDTO) (evt.getComponent().getAttributes()
                                                 .get("selectedEmpleado"));

            Long empId = new Long(selectedEmpleado.getEmpId());
            entity = businessDelegatorView.getEmpleado(empId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long empId = FacesUtils.checkLong(txtEmpId);
            entity = businessDelegatorView.getEmpleado(empId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteEmpleado(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, String apellido,
        Long cedula, Long celular, String correo, String direccion, Long empId,
        Date fechaCreacion, Date fechaModificacion, Date fechaNacimiento,
        String foto, String nombre, String password, String sexo,
        String usuarioCreador, String usuarioModificador,
        Long tiemId_TipoEmpleado) throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setApellido(FacesUtils.checkString(apellido));
            entity.setCedula(FacesUtils.checkLong(cedula));
            entity.setCelular(FacesUtils.checkLong(celular));
            entity.setCorreo(FacesUtils.checkString(correo));
            entity.setDireccion(FacesUtils.checkString(direccion));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(fechaNacimiento));
            entity.setFoto(FacesUtils.checkString(foto));
            entity.setNombre(FacesUtils.checkString(nombre));
            entity.setPassword(FacesUtils.checkString(password));
            entity.setSexo(FacesUtils.checkString(sexo));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateEmpleado(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("EmpleadoView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtApellido() {
        return txtApellido;
    }

    public void setTxtApellido(InputText txtApellido) {
        this.txtApellido = txtApellido;
    }

    public InputText getTxtCedula() {
        return txtCedula;
    }

    public void setTxtCedula(InputText txtCedula) {
        this.txtCedula = txtCedula;
    }

    public InputText getTxtCelular() {
        return txtCelular;
    }

    public void setTxtCelular(InputText txtCelular) {
        this.txtCelular = txtCelular;
    }

    public InputText getTxtCorreo() {
        return txtCorreo;
    }

    public void setTxtCorreo(InputText txtCorreo) {
        this.txtCorreo = txtCorreo;
    }

    public InputText getTxtDireccion() {
        return txtDireccion;
    }

    public void setTxtDireccion(InputText txtDireccion) {
        this.txtDireccion = txtDireccion;
    }

    public InputText getTxtFoto() {
        return txtFoto;
    }

    public void setTxtFoto(InputText txtFoto) {
        this.txtFoto = txtFoto;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(InputText txtPassword) {
        this.txtPassword = txtPassword;
    }

    public InputText getTxtSexo() {
        return txtSexo;
    }

    public void setTxtSexo(InputText txtSexo) {
        this.txtSexo = txtSexo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtTiemId_TipoEmpleado() {
        return txtTiemId_TipoEmpleado;
    }

    public void setTxtTiemId_TipoEmpleado(InputText txtTiemId_TipoEmpleado) {
        this.txtTiemId_TipoEmpleado = txtTiemId_TipoEmpleado;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public Calendar getTxtFechaNacimiento() {
        return txtFechaNacimiento;
    }

    public void setTxtFechaNacimiento(Calendar txtFechaNacimiento) {
        this.txtFechaNacimiento = txtFechaNacimiento;
    }

    public InputText getTxtEmpId() {
        return txtEmpId;
    }

    public void setTxtEmpId(InputText txtEmpId) {
        this.txtEmpId = txtEmpId;
    }

    public List<EmpleadoDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataEmpleado();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<EmpleadoDTO> empleadoDTO) {
        this.data = empleadoDTO;
    }

    public EmpleadoDTO getSelectedEmpleado() {
        return selectedEmpleado;
    }

    public void setSelectedEmpleado(EmpleadoDTO empleado) {
        this.selectedEmpleado = empleado;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
