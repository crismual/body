package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import co.edu.usbcali.bodyfitness.exceptions.*;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.*;
import co.edu.usbcali.bodyfitness.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class PagoView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PagoView.class);
    private InputText txtActivo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtValor;
    private InputText txtPlancliId_PlanCliente;
    private InputText txtTipaId_TipoPago;
    private InputText txtPagId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private Calendar txtFechaPago;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<PagoDTO> data;
    private PagoDTO selectedPago;
    private Pago entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public PagoView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedPago = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedPago = null;

        if (txtActivo != null) {
            txtActivo.setValue(null);
            txtActivo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtValor != null) {
            txtValor.setValue(null);
            txtValor.setDisabled(true);
        }

        if (txtPlancliId_PlanCliente != null) {
            txtPlancliId_PlanCliente.setValue(null);
            txtPlancliId_PlanCliente.setDisabled(true);
        }

        if (txtTipaId_TipoPago != null) {
            txtTipaId_TipoPago.setValue(null);
            txtTipaId_TipoPago.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtFechaPago != null) {
            txtFechaPago.setValue(null);
            txtFechaPago.setDisabled(true);
        }

        if (txtPagId != null) {
            txtPagId.setValue(null);
            txtPagId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaPago() {
        Date inputDate = (Date) txtFechaPago.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long pagId = FacesUtils.checkLong(txtPagId);
            entity = (pagId != null) ? businessDelegatorView.getPago(pagId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtActivo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtValor.setDisabled(false);
            txtPlancliId_PlanCliente.setDisabled(false);
            txtTipaId_TipoPago.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtFechaPago.setDisabled(false);
            txtPagId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtFechaPago.setValue(entity.getFechaPago());
            txtFechaPago.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtValor.setValue(entity.getValor());
            txtValor.setDisabled(false);
            txtPlancliId_PlanCliente.setValue(entity.getPlanCliente()
                                                    .getPlancliId());
            txtPlancliId_PlanCliente.setDisabled(false);
            txtTipaId_TipoPago.setValue(entity.getTipoPago().getTipaId());
            txtTipaId_TipoPago.setDisabled(false);
            txtPagId.setValue(entity.getPagId());
            txtPagId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedPago = (PagoDTO) (evt.getComponent().getAttributes()
                                     .get("selectedPago"));
        txtActivo.setValue(selectedPago.getActivo());
        txtActivo.setDisabled(false);
        txtFechaCreacion.setValue(selectedPago.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedPago.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtFechaPago.setValue(selectedPago.getFechaPago());
        txtFechaPago.setDisabled(false);
        txtUsuarioCreador.setValue(selectedPago.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedPago.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtValor.setValue(selectedPago.getValor());
        txtValor.setDisabled(false);
        txtPlancliId_PlanCliente.setValue(selectedPago.getPlancliId_PlanCliente());
        txtPlancliId_PlanCliente.setDisabled(false);
        txtTipaId_TipoPago.setValue(selectedPago.getTipaId_TipoPago());
        txtTipaId_TipoPago.setDisabled(false);
        txtPagId.setValue(selectedPago.getPagId());
        txtPagId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedPago == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Pago();

            Long pagId = FacesUtils.checkLong(txtPagId);

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaPago(FacesUtils.checkDate(txtFechaPago));
            entity.setPagId(pagId);
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setValor(FacesUtils.checkLong(txtValor));
            entity.setPlanCliente((FacesUtils.checkLong(
                    txtPlancliId_PlanCliente) != null)
                ? businessDelegatorView.getPlanCliente(FacesUtils.checkLong(
                        txtPlancliId_PlanCliente)) : null);
            entity.setTipoPago((FacesUtils.checkLong(txtTipaId_TipoPago) != null)
                ? businessDelegatorView.getTipoPago(FacesUtils.checkLong(
                        txtTipaId_TipoPago)) : null);
            businessDelegatorView.savePago(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long pagId = new Long(selectedPago.getPagId());
                entity = businessDelegatorView.getPago(pagId);
            }

            entity.setActivo(FacesUtils.checkString(txtActivo));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaPago(FacesUtils.checkDate(txtFechaPago));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setValor(FacesUtils.checkLong(txtValor));
            entity.setPlanCliente((FacesUtils.checkLong(
                    txtPlancliId_PlanCliente) != null)
                ? businessDelegatorView.getPlanCliente(FacesUtils.checkLong(
                        txtPlancliId_PlanCliente)) : null);
            entity.setTipoPago((FacesUtils.checkLong(txtTipaId_TipoPago) != null)
                ? businessDelegatorView.getTipoPago(FacesUtils.checkLong(
                        txtTipaId_TipoPago)) : null);
            businessDelegatorView.updatePago(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedPago = (PagoDTO) (evt.getComponent().getAttributes()
                                         .get("selectedPago"));

            Long pagId = new Long(selectedPago.getPagId());
            entity = businessDelegatorView.getPago(pagId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long pagId = FacesUtils.checkLong(txtPagId);
            entity = businessDelegatorView.getPago(pagId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deletePago(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Date fechaCreacion,
        Date fechaModificacion, Date fechaPago, Long pagId,
        String usuarioCreador, String usuarioModificador, Long valor,
        Long plancliId_PlanCliente, Long tipaId_TipoPago)
        throws Exception {
        try {
            entity.setActivo(FacesUtils.checkString(activo));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setFechaPago(FacesUtils.checkDate(fechaPago));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            entity.setValor(FacesUtils.checkLong(valor));
            businessDelegatorView.updatePago(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("PagoView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(InputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtValor() {
        return txtValor;
    }

    public void setTxtValor(InputText txtValor) {
        this.txtValor = txtValor;
    }

    public InputText getTxtPlancliId_PlanCliente() {
        return txtPlancliId_PlanCliente;
    }

    public void setTxtPlancliId_PlanCliente(InputText txtPlancliId_PlanCliente) {
        this.txtPlancliId_PlanCliente = txtPlancliId_PlanCliente;
    }

    public InputText getTxtTipaId_TipoPago() {
        return txtTipaId_TipoPago;
    }

    public void setTxtTipaId_TipoPago(InputText txtTipaId_TipoPago) {
        this.txtTipaId_TipoPago = txtTipaId_TipoPago;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public Calendar getTxtFechaPago() {
        return txtFechaPago;
    }

    public void setTxtFechaPago(Calendar txtFechaPago) {
        this.txtFechaPago = txtFechaPago;
    }

    public InputText getTxtPagId() {
        return txtPagId;
    }

    public void setTxtPagId(InputText txtPagId) {
        this.txtPagId = txtPagId;
    }

    public List<PagoDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataPago();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<PagoDTO> pagoDTO) {
        this.data = pagoDTO;
    }

    public PagoDTO getSelectedPago() {
        return selectedPago;
    }

    public void setSelectedPago(PagoDTO pago) {
        this.selectedPago = pago;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
