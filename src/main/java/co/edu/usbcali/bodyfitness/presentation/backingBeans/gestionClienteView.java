package co.edu.usbcali.bodyfitness.presentation.backingBeans;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.usbcali.bodyfitness.exceptions.ZMessManager;
import co.edu.usbcali.bodyfitness.modelo.Cliente;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;
import co.edu.usbcali.bodyfitness.utilities.FacesUtils;

@ManagedBean
@ViewScoped
public class gestionClienteView {
	
	private static final Logger log = LoggerFactory.getLogger(gestionClienteView.class);
	private SelectOneMenu somActivo;
    private InputText txtApellido;
    private InputText txtCedula;
    private InputText txtCelular;
    private InputText txtDireccion;
    private InputText txtCorreo;
    private InputText txtFoto;
    private InputText txtNombre;
    private InputText txtPassword;
    private SelectOneMenu somSexo;
    private Calendar txtFechaNacimiento;
    public InputText getTxtDireccion() {
		return txtDireccion;
	}

	public void setTxtDireccion(InputText txtDireccion) {
		this.txtDireccion = txtDireccion;
	}
	private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnClear;
    private List<ClienteDTO> data;
    private ClienteDTO selectedCliente;
    private Cliente entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;
    
    
    public void HabilitarCampos() throws Exception{
    	
	    txtApellido.setDisabled(false);
	    txtCedula.setDisabled(false);
	    txtNombre.setDisabled(false);
	    somSexo.setDisabled(false);
	    txtFechaNacimiento.setDisabled(false);
    	
    }
    
    public void DeshabilitarCampos() throws Exception{
    	
	    txtApellido.setDisabled(true);
	    txtCedula.setDisabled(true);
	    txtNombre.setDisabled(true);
	    somSexo.setDisabled(true);
	    txtFechaNacimiento.setDisabled(true);
    	
    }
    
    public void txtIdentificacionListener() throws Exception{
    	
    	Long cedula = null;
    	
    	try {
    		
    		cedula = new Long(txtCedula.getValue().toString());
    		
		} catch (Exception e) {
			
			FacesMessage mensaje = new FacesMessage(FacesMessage.SEVERITY_ERROR, "La identificación debe ser numérica", "");
			FacesContext.getCurrentInstance().addMessage("", mensaje);
		}
    	
    	Cliente cliente = businessDelegatorView.BuscarClientePorCedula(cedula);
    	
    	if(cliente == null) {

    	    txtApellido.resetValue();
    	    txtCelular.resetValue();
    	    txtCorreo.resetValue();
    	    txtDireccion.resetValue();
    	    txtNombre.resetValue();
    	    somSexo.resetValue();
    	    txtFechaNacimiento.resetValue();
    	    
    	    HabilitarCampos();
    		
    		
    	    btnModify.setDisabled(true);
    		btnSave.setDisabled(false);
    		
    	}else {
    		
    		txtApellido.setValue(cliente.getApellido());
    	    txtCedula.setValue(cliente.getCedula());
    	    txtCelular.setValue(cliente.getCelular());
    	    txtCorreo.setValue(cliente.getCorreo());
    	    txtNombre.setValue(cliente.getNombre());
    	    txtDireccion.setValue(cliente.getDireccion());
    	    somSexo.setValue(cliente.getSexo());
    	    txtFechaNacimiento.setValue(cliente.getFechaNacimiento());
    	    
           DeshabilitarCampos();
    	    
    	    
    	    btnModify.setDisabled(false);
    	    btnSave.setDisabled(true);
    	}
    	
    	
    }
    
    private String generarPassword() throws NoSuchAlgorithmException {

		String[] symbols = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
		int length = 10;
		Random random = SecureRandom.getInstanceStrong(); // as of JDK 8, this should return the strongest algorithm available to the JVM
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
		    int indexRandom = random.nextInt( symbols.length );
		    sb.append( symbols[indexRandom] );
		}
		
		String password = sb.toString();
		
	
	return password;
	
}
    
	public String CrearAction() {
		
        try {
            entity = new Cliente();

            entity.setActivo("S");
            entity.setApellido(txtApellido.getValue().toString());
            entity.setCedula(Long.parseLong(txtCedula.getValue().toString()));
            entity.setCelular(txtCelular.getValue().toString());
            entity.setCorreo(txtCorreo.getValue().toString());
            entity.setDireccion(txtDireccion.getValue().toString());
            entity.setFechaCreacion(new Date());
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setFoto("src/main/webapp/images/usuarios/userImg.png");
            entity.setNombre(txtNombre.getValue().toString());
            entity.setPassword(generarPassword());
            entity.setSexo(somSexo.getLocalValue().toString());
            entity.setUsuarioCreador("Default user");
            businessDelegatorView.saveCliente(entity);
            FacesUtils.addInfoMessage("Cliente creado con exito");
            limpiarAction();
            HabilitarCampos();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage("ERROR AL GUARDAR : "+e.getMessage());
        }

        return "";
		
	}
	
	public String modificarAction() throws NumberFormatException, Exception {
		
        entity = businessDelegatorView.BuscarClientePorCedula(Long.parseLong(txtCedula.getValue().toString()));
		
        try {

            entity.setActivo("S");
            entity.setApellido(txtApellido.getValue().toString());
            entity.setCedula(Long.parseLong(txtCedula.getValue().toString()));
            entity.setCelular(txtCelular.getValue().toString());
            entity.setCorreo(txtCorreo.getValue().toString());
            entity.setDireccion(txtDireccion.getValue().toString());
            entity.setFechaCreacion(new Date());
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setFoto("src/main/webapp/images/usuarios/userImg.png");
            entity.setNombre(txtNombre.getValue().toString());
            entity.setPassword(generarPassword());
            entity.setSexo(somSexo.getLocalValue().toString());
            entity.setUsuarioCreador("Default user");
            businessDelegatorView.updateCliente(entity);
            FacesUtils.addInfoMessage("Cliente modificado con exito");
            limpiarAction();
            HabilitarCampos();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage("ERROR AL GUARDAR : "+e.getMessage());
        }

        return "";

	}
	
	public String limpiarAction() throws Exception {
		

	    txtApellido.resetValue();
	    txtCedula.resetValue();
	    txtCelular.resetValue();
	    txtCorreo.resetValue();
	    txtDireccion.resetValue();
	    txtNombre.resetValue();
	    somSexo.resetValue();
	    txtFechaNacimiento.resetValue();
		
		
	    btnModify.setDisabled(true);
		btnSave.setDisabled(true);

		HabilitarCampos();
		
		return "";
	}
	
	public void listener_txtFechaNacimiento() {
        Date inputDate = (Date) txtFechaNacimiento.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }
	

    
	public SelectOneMenu getSomActivo() {
		return somActivo;
	}
	public void setSomActivo(SelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}
	public InputText getTxtApellido() {
		return txtApellido;
	}
	public void setTxtApellido(InputText txtApellido) {
		this.txtApellido = txtApellido;
	}
	public InputText getTxtCedula() {
		return txtCedula;
	}
	public void setTxtCedula(InputText txtCedula) {
		this.txtCedula = txtCedula;
	}
	public InputText getTxtCelular() {
		return txtCelular;
	}
	public void setTxtCelular(InputText txtCelular) {
		this.txtCelular = txtCelular;
	}
	public InputText getTxtCorreo() {
		return txtCorreo;
	}
	public void setTxtCorreo(InputText txtCorreo) {
		this.txtCorreo = txtCorreo;
	}
	public InputText getTxtFoto() {
		return txtFoto;
	}
	public void setTxtFoto(InputText txtFoto) {
		this.txtFoto = txtFoto;
	}
	public InputText getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(InputText txtNombre) {
		this.txtNombre = txtNombre;
	}
	public InputText getTxtPassword() {
		return txtPassword;
	}
	public void setTxtPassword(InputText txtPassword) {
		this.txtPassword = txtPassword;
	}
	
	public SelectOneMenu getSomSexo() {
		return somSexo;
	}

	public void setSomSexo(SelectOneMenu somSexo) {
		this.somSexo = somSexo;
	}

	public Calendar getTxtFechaNacimiento() {
		return txtFechaNacimiento;
	}
	public void setTxtFechaNacimiento(Calendar txtFechaNacimiento) {
		this.txtFechaNacimiento = txtFechaNacimiento;
	}
	public CommandButton getBtnSave() {
		return btnSave;
	}
	public void setBtnSave(CommandButton btnSave) {
		this.btnSave = btnSave;
	}
	public CommandButton getBtnModify() {
		return btnModify;
	}
	public void setBtnModify(CommandButton btnModify) {
		this.btnModify = btnModify;
	}
	public CommandButton getBtnClear() {
		return btnClear;
	}
	public void setBtnClear(CommandButton btnClear) {
		this.btnClear = btnClear;
	}
	public List<ClienteDTO> getData() {
		return data;
	}
	public void setData(List<ClienteDTO> data) {
		this.data = data;
	}
	public ClienteDTO getSelectedCliente() {
		return selectedCliente;
	}
	public void setSelectedCliente(ClienteDTO selectedCliente) {
		this.selectedCliente = selectedCliente;
	}
	public Cliente getEntity() {
		return entity;
	}
	public void setEntity(Cliente entity) {
		this.entity = entity;
	}
	public boolean isShowDialog() {
		return showDialog;
	}
	public void setShowDialog(boolean showDialog) {
		this.showDialog = showDialog;
	}
	public IBusinessDelegatorView getBusinessDelegatorView() {
		return businessDelegatorView;
	}
	public void setBusinessDelegatorView(IBusinessDelegatorView businessDelegatorView) {
		this.businessDelegatorView = businessDelegatorView;
	}
	public static Logger getLog() {
		return log;
	}
}
