package co.edu.usbcali.bodyfitness.presentation.businessDelegate;

import co.edu.usbcali.bodyfitness.modelo.Antropometria;
import co.edu.usbcali.bodyfitness.modelo.Cliente;
import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;
import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;
import co.edu.usbcali.bodyfitness.modelo.Empleado;
import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;
import co.edu.usbcali.bodyfitness.modelo.Rutina;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;
import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.modelo.control.AntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EjercicioRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEjercicioRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPlanClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPlanLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PlanClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PlanLogic;
import co.edu.usbcali.bodyfitness.modelo.control.RutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEjercicioDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEmpleadoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoPagoDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.sql.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Use a Business Delegate to reduce coupling between presentation-tier clients and business services.
* The Business Delegate hides the underlying implementation details of the business service, such as lookup and access details of the EJB architecture.
*
* The Business Delegate acts as a client-side business abstraction; it provides an abstraction for, and thus hides,
* the implementation of the business services. Using a Business Delegate reduces the coupling between presentation-tier clients and
* the system's business services. Depending on the implementation strategy, the Business Delegate may shield clients from possible
* volatility in the implementation of the business service API. Potentially, this reduces the number of changes that must be made to the
* presentation-tier client code when the business service API or its underlying implementation changes.
*
* However, interface methods in the Business Delegate may still require modification if the underlying business service API changes.
* Admittedly, though, it is more likely that changes will be made to the business service rather than to the Business Delegate.
*
* Often, developers are skeptical when a design goal such as abstracting the business layer causes additional upfront work in return
* for future gains. However, using this pattern or its strategies results in only a small amount of additional upfront work and provides
* considerable benefits. The main benefit is hiding the details of the underlying service. For example, the client can become transparent
* to naming and lookup services. The Business Delegate also handles the exceptions from the business services, such as java.rmi.Remote
* exceptions, Java Messages Service (JMS) exceptions and so on. The Business Delegate may intercept such service level exceptions and
* generate application level exceptions instead. Application level exceptions are easier to handle by the clients, and may be user friendly.
* The Business Delegate may also transparently perform any retry or recovery operations necessary in the event of a service failure without
* exposing the client to the problem until it is determined that the problem is not resolvable. These gains present a compelling reason to
* use the pattern.
*
* Another benefit is that the delegate may cache results and references to remote business services. Caching can significantly improve performance,
* because it limits unnecessary and potentially costly round trips over the network.
*
* A Business Delegate uses a component called the Lookup Service. The Lookup Service is responsible for hiding the underlying implementation
* details of the business service lookup code. The Lookup Service may be written as part of the Delegate, but we recommend that it be
* implemented as a separate component, as outlined in the Service Locator pattern (See "Service Locator" on page 368.)
*
* When the Business Delegate is used with a Session Facade, typically there is a one-to-one relationship between the two.
* This one-to-one relationship exists because logic that might have been encapsulated in a Business Delegate relating to its interaction
* with multiple business services (creating a one-to-many relationship) will often be factored back into a Session Facade.
*
* Finally, it should be noted that this pattern could be used to reduce coupling between other tiers, not simply the presentation and the
* business tiers.
*
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Scope("singleton")
@Service("BusinessDelegatorView")
public class BusinessDelegatorView implements IBusinessDelegatorView {
    private static final Logger log = LoggerFactory.getLogger(BusinessDelegatorView.class);
    @Autowired
    private IAntropometriaLogic antropometriaLogic;
    @Autowired
    private IClienteLogic clienteLogic;
    @Autowired
    private IClienteAntropometriaLogic clienteAntropometriaLogic;
    @Autowired
    private IClienteRutinaLogic clienteRutinaLogic;
    @Autowired
    private IEjercicioLogic ejercicioLogic;
    @Autowired
    private IEjercicioRutinaLogic ejercicioRutinaLogic;
    @Autowired
    private IEmpleadoLogic empleadoLogic;
    @Autowired
    private IPagoLogic pagoLogic;
    @Autowired
    private IPlanLogic planLogic;
    @Autowired
    private IPlanClienteLogic planClienteLogic;
    @Autowired
    private IRutinaLogic rutinaLogic;
    @Autowired
    private ITipoEjercicioLogic tipoEjercicioLogic;
    @Autowired
    private ITipoEmpleadoLogic tipoEmpleadoLogic;
    @Autowired
    private ITipoPagoLogic tipoPagoLogic;

    public List<Antropometria> getAntropometria() throws Exception {
        return antropometriaLogic.getAntropometria();
    }

    public void saveAntropometria(Antropometria entity)
        throws Exception {
        antropometriaLogic.saveAntropometria(entity);
    }

    public void deleteAntropometria(Antropometria entity)
        throws Exception {
        antropometriaLogic.deleteAntropometria(entity);
    }

    public void updateAntropometria(Antropometria entity)
        throws Exception {
        antropometriaLogic.updateAntropometria(entity);
    }

    public Antropometria getAntropometria(Long antId) throws Exception {
        Antropometria antropometria = null;

        try {
            antropometria = antropometriaLogic.getAntropometria(antId);
        } catch (Exception e) {
            throw e;
        }

        return antropometria;
    }

    public List<Antropometria> findByCriteriaInAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return antropometriaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Antropometria> findPageAntropometria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return antropometriaLogic.findPageAntropometria(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberAntropometria() throws Exception {
        return antropometriaLogic.findTotalNumberAntropometria();
    }

    public List<AntropometriaDTO> getDataAntropometria()
        throws Exception {
        return antropometriaLogic.getDataAntropometria();
    }

    public void validateAntropometria(Antropometria antropometria)
        throws Exception {
        antropometriaLogic.validateAntropometria(antropometria);
    }

    public List<Cliente> getCliente() throws Exception {
        return clienteLogic.getCliente();
    }

    public void saveCliente(Cliente entity) throws Exception {
        clienteLogic.saveCliente(entity);
    }

    public void deleteCliente(Cliente entity) throws Exception {
        clienteLogic.deleteCliente(entity);
    }

    public void updateCliente(Cliente entity) throws Exception {
        clienteLogic.updateCliente(entity);
    }

    public Cliente getCliente(Long cliId) throws Exception {
        Cliente cliente = null;

        try {
            cliente = clienteLogic.getCliente(cliId);
        } catch (Exception e) {
            throw e;
        }

        return cliente;
    }

    public List<Cliente> findByCriteriaInCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return clienteLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Cliente> findPageCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return clienteLogic.findPageCliente(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberCliente() throws Exception {
        return clienteLogic.findTotalNumberCliente();
    }

    public List<ClienteDTO> getDataCliente() throws Exception {
        return clienteLogic.getDataCliente();
    }

    public void validateCliente(Cliente cliente) throws Exception {
        clienteLogic.validateCliente(cliente);
    }

    public List<ClienteAntropometria> getClienteAntropometria()
        throws Exception {
        return clienteAntropometriaLogic.getClienteAntropometria();
    }

    public void saveClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaLogic.saveClienteAntropometria(entity);
    }

    public void deleteClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaLogic.deleteClienteAntropometria(entity);
    }

    public void updateClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaLogic.updateClienteAntropometria(entity);
    }

    public ClienteAntropometria getClienteAntropometria(Long cliantId)
        throws Exception {
        ClienteAntropometria clienteAntropometria = null;

        try {
            clienteAntropometria = clienteAntropometriaLogic.getClienteAntropometria(cliantId);
        } catch (Exception e) {
            throw e;
        }

        return clienteAntropometria;
    }

    public List<ClienteAntropometria> findByCriteriaInClienteAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return clienteAntropometriaLogic.findByCriteria(variables,
            variablesBetween, variablesBetweenDates);
    }

    public List<ClienteAntropometria> findPageClienteAntropometria(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return clienteAntropometriaLogic.findPageClienteAntropometria(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberClienteAntropometria() throws Exception {
        return clienteAntropometriaLogic.findTotalNumberClienteAntropometria();
    }

    public List<ClienteAntropometriaDTO> getDataClienteAntropometria()
        throws Exception {
        return clienteAntropometriaLogic.getDataClienteAntropometria();
    }

    public void validateClienteAntropometria(
        ClienteAntropometria clienteAntropometria) throws Exception {
        clienteAntropometriaLogic.validateClienteAntropometria(clienteAntropometria);
    }

    public List<ClienteRutina> getClienteRutina() throws Exception {
        return clienteRutinaLogic.getClienteRutina();
    }

    public void saveClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaLogic.saveClienteRutina(entity);
    }

    public void deleteClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaLogic.deleteClienteRutina(entity);
    }

    public void updateClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaLogic.updateClienteRutina(entity);
    }

    public ClienteRutina getClienteRutina(Long clirutId)
        throws Exception {
        ClienteRutina clienteRutina = null;

        try {
            clienteRutina = clienteRutinaLogic.getClienteRutina(clirutId);
        } catch (Exception e) {
            throw e;
        }

        return clienteRutina;
    }

    public List<ClienteRutina> findByCriteriaInClienteRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return clienteRutinaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<ClienteRutina> findPageClienteRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return clienteRutinaLogic.findPageClienteRutina(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberClienteRutina() throws Exception {
        return clienteRutinaLogic.findTotalNumberClienteRutina();
    }

    public List<ClienteRutinaDTO> getDataClienteRutina()
        throws Exception {
        return clienteRutinaLogic.getDataClienteRutina();
    }

    public void validateClienteRutina(ClienteRutina clienteRutina)
        throws Exception {
        clienteRutinaLogic.validateClienteRutina(clienteRutina);
    }

    public List<Ejercicio> getEjercicio() throws Exception {
        return ejercicioLogic.getEjercicio();
    }

    public void saveEjercicio(Ejercicio entity) throws Exception {
        ejercicioLogic.saveEjercicio(entity);
    }

    public void deleteEjercicio(Ejercicio entity) throws Exception {
        ejercicioLogic.deleteEjercicio(entity);
    }

    public void updateEjercicio(Ejercicio entity) throws Exception {
        ejercicioLogic.updateEjercicio(entity);
    }

    public Ejercicio getEjercicio(Long ejeId) throws Exception {
        Ejercicio ejercicio = null;

        try {
            ejercicio = ejercicioLogic.getEjercicio(ejeId);
        } catch (Exception e) {
            throw e;
        }

        return ejercicio;
    }

    public List<Ejercicio> findByCriteriaInEjercicio(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return ejercicioLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Ejercicio> findPageEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return ejercicioLogic.findPageEjercicio(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberEjercicio() throws Exception {
        return ejercicioLogic.findTotalNumberEjercicio();
    }

    public List<EjercicioDTO> getDataEjercicio() throws Exception {
        return ejercicioLogic.getDataEjercicio();
    }

    public void validateEjercicio(Ejercicio ejercicio)
        throws Exception {
        ejercicioLogic.validateEjercicio(ejercicio);
    }

    public List<EjercicioRutina> getEjercicioRutina() throws Exception {
        return ejercicioRutinaLogic.getEjercicioRutina();
    }

    public void saveEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaLogic.saveEjercicioRutina(entity);
    }

    public void deleteEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaLogic.deleteEjercicioRutina(entity);
    }

    public void updateEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaLogic.updateEjercicioRutina(entity);
    }

    public EjercicioRutina getEjercicioRutina(Long ejerutId)
        throws Exception {
        EjercicioRutina ejercicioRutina = null;

        try {
            ejercicioRutina = ejercicioRutinaLogic.getEjercicioRutina(ejerutId);
        } catch (Exception e) {
            throw e;
        }

        return ejercicioRutina;
    }

    public List<EjercicioRutina> findByCriteriaInEjercicioRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return ejercicioRutinaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<EjercicioRutina> findPageEjercicioRutina(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return ejercicioRutinaLogic.findPageEjercicioRutina(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberEjercicioRutina() throws Exception {
        return ejercicioRutinaLogic.findTotalNumberEjercicioRutina();
    }

    public List<EjercicioRutinaDTO> getDataEjercicioRutina()
        throws Exception {
        return ejercicioRutinaLogic.getDataEjercicioRutina();
    }

    public void validateEjercicioRutina(EjercicioRutina ejercicioRutina)
        throws Exception {
        ejercicioRutinaLogic.validateEjercicioRutina(ejercicioRutina);
    }

    public List<Empleado> getEmpleado() throws Exception {
        return empleadoLogic.getEmpleado();
    }

    public void saveEmpleado(Empleado entity) throws Exception {
        empleadoLogic.saveEmpleado(entity);
    }

    public void deleteEmpleado(Empleado entity) throws Exception {
        empleadoLogic.deleteEmpleado(entity);
    }

    public void updateEmpleado(Empleado entity) throws Exception {
        empleadoLogic.updateEmpleado(entity);
    }

    public Empleado getEmpleado(Long empId) throws Exception {
        Empleado empleado = null;

        try {
            empleado = empleadoLogic.getEmpleado(empId);
        } catch (Exception e) {
            throw e;
        }

        return empleado;
    }

    public List<Empleado> findByCriteriaInEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return empleadoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Empleado> findPageEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return empleadoLogic.findPageEmpleado(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberEmpleado() throws Exception {
        return empleadoLogic.findTotalNumberEmpleado();
    }

    public List<EmpleadoDTO> getDataEmpleado() throws Exception {
        return empleadoLogic.getDataEmpleado();
    }

    public void validateEmpleado(Empleado empleado) throws Exception {
        empleadoLogic.validateEmpleado(empleado);
    }

    public List<Pago> getPago() throws Exception {
        return pagoLogic.getPago();
    }

    public void savePago(Pago entity) throws Exception {
        pagoLogic.savePago(entity);
    }

    public void deletePago(Pago entity) throws Exception {
        pagoLogic.deletePago(entity);
    }

    public void updatePago(Pago entity) throws Exception {
        pagoLogic.updatePago(entity);
    }

    public Pago getPago(Long pagId) throws Exception {
        Pago pago = null;

        try {
            pago = pagoLogic.getPago(pagId);
        } catch (Exception e) {
            throw e;
        }

        return pago;
    }

    public List<Pago> findByCriteriaInPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return pagoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Pago> findPagePago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return pagoLogic.findPagePago(sortColumnName, sortAscending, startRow,
            maxResults);
    }

    public Long findTotalNumberPago() throws Exception {
        return pagoLogic.findTotalNumberPago();
    }

    public List<PagoDTO> getDataPago() throws Exception {
        return pagoLogic.getDataPago();
    }

    public void validatePago(Pago pago) throws Exception {
        pagoLogic.validatePago(pago);
    }

    public List<Plan> getPlan() throws Exception {
        return planLogic.getPlan();
    }

    public void savePlan(Plan entity) throws Exception {
        planLogic.savePlan(entity);
    }

    public void deletePlan(Plan entity) throws Exception {
        planLogic.deletePlan(entity);
    }

    public void updatePlan(Plan entity) throws Exception {
        planLogic.updatePlan(entity);
    }

    public Plan getPlan(Long plaId) throws Exception {
        Plan plan = null;

        try {
            plan = planLogic.getPlan(plaId);
        } catch (Exception e) {
            throw e;
        }

        return plan;
    }

    public List<Plan> findByCriteriaInPlan(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return planLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Plan> findPagePlan(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return planLogic.findPagePlan(sortColumnName, sortAscending, startRow,
            maxResults);
    }

    public Long findTotalNumberPlan() throws Exception {
        return planLogic.findTotalNumberPlan();
    }

    public List<PlanDTO> getDataPlan() throws Exception {
        return planLogic.getDataPlan();
    }

    public void validatePlan(Plan plan) throws Exception {
        planLogic.validatePlan(plan);
    }

    public List<PlanCliente> getPlanCliente() throws Exception {
        return planClienteLogic.getPlanCliente();
    }

    public void savePlanCliente(PlanCliente entity) throws Exception {
        planClienteLogic.savePlanCliente(entity);
    }

    public void deletePlanCliente(PlanCliente entity) throws Exception {
        planClienteLogic.deletePlanCliente(entity);
    }

    public void updatePlanCliente(PlanCliente entity) throws Exception {
        planClienteLogic.updatePlanCliente(entity);
    }

    public PlanCliente getPlanCliente(Long plancliId) throws Exception {
        PlanCliente planCliente = null;

        try {
            planCliente = planClienteLogic.getPlanCliente(plancliId);
        } catch (Exception e) {
            throw e;
        }

        return planCliente;
    }

    public List<PlanCliente> findByCriteriaInPlanCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return planClienteLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<PlanCliente> findPagePlanCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return planClienteLogic.findPagePlanCliente(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberPlanCliente() throws Exception {
        return planClienteLogic.findTotalNumberPlanCliente();
    }

    public List<PlanClienteDTO> getDataPlanCliente() throws Exception {
        return planClienteLogic.getDataPlanCliente();
    }

    public void validatePlanCliente(PlanCliente planCliente)
        throws Exception {
        planClienteLogic.validatePlanCliente(planCliente);
    }

    public List<Rutina> getRutina() throws Exception {
        return rutinaLogic.getRutina();
    }

    public void saveRutina(Rutina entity) throws Exception {
        rutinaLogic.saveRutina(entity);
    }

    public void deleteRutina(Rutina entity) throws Exception {
        rutinaLogic.deleteRutina(entity);
    }

    public void updateRutina(Rutina entity) throws Exception {
        rutinaLogic.updateRutina(entity);
    }

    public Rutina getRutina(Long rutId) throws Exception {
        Rutina rutina = null;

        try {
            rutina = rutinaLogic.getRutina(rutId);
        } catch (Exception e) {
            throw e;
        }

        return rutina;
    }

    public List<Rutina> findByCriteriaInRutina(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return rutinaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Rutina> findPageRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return rutinaLogic.findPageRutina(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberRutina() throws Exception {
        return rutinaLogic.findTotalNumberRutina();
    }

    public List<RutinaDTO> getDataRutina() throws Exception {
        return rutinaLogic.getDataRutina();
    }

    public void validateRutina(Rutina rutina) throws Exception {
        rutinaLogic.validateRutina(rutina);
    }

    public List<TipoEjercicio> getTipoEjercicio() throws Exception {
        return tipoEjercicioLogic.getTipoEjercicio();
    }

    public void saveTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioLogic.saveTipoEjercicio(entity);
    }

    public void deleteTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioLogic.deleteTipoEjercicio(entity);
    }

    public void updateTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioLogic.updateTipoEjercicio(entity);
    }

    public TipoEjercicio getTipoEjercicio(Long tiejeId)
        throws Exception {
        TipoEjercicio tipoEjercicio = null;

        try {
            tipoEjercicio = tipoEjercicioLogic.getTipoEjercicio(tiejeId);
        } catch (Exception e) {
            throw e;
        }

        return tipoEjercicio;
    }

    public List<TipoEjercicio> findByCriteriaInTipoEjercicio(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return tipoEjercicioLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoEjercicio> findPageTipoEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoEjercicioLogic.findPageTipoEjercicio(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberTipoEjercicio() throws Exception {
        return tipoEjercicioLogic.findTotalNumberTipoEjercicio();
    }

    public List<TipoEjercicioDTO> getDataTipoEjercicio()
        throws Exception {
        return tipoEjercicioLogic.getDataTipoEjercicio();
    }

    public void validateTipoEjercicio(TipoEjercicio tipoEjercicio)
        throws Exception {
        tipoEjercicioLogic.validateTipoEjercicio(tipoEjercicio);
    }

    public List<TipoEmpleado> getTipoEmpleado() throws Exception {
        return tipoEmpleadoLogic.getTipoEmpleado();
    }

    public void saveTipoEmpleado(TipoEmpleado entity) throws Exception {
        tipoEmpleadoLogic.saveTipoEmpleado(entity);
    }

    public void deleteTipoEmpleado(TipoEmpleado entity)
        throws Exception {
        tipoEmpleadoLogic.deleteTipoEmpleado(entity);
    }

    public void updateTipoEmpleado(TipoEmpleado entity)
        throws Exception {
        tipoEmpleadoLogic.updateTipoEmpleado(entity);
    }

    public TipoEmpleado getTipoEmpleado(Long tiemId) throws Exception {
        TipoEmpleado tipoEmpleado = null;

        try {
            tipoEmpleado = tipoEmpleadoLogic.getTipoEmpleado(tiemId);
        } catch (Exception e) {
            throw e;
        }

        return tipoEmpleado;
    }

    public List<TipoEmpleado> findByCriteriaInTipoEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return tipoEmpleadoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoEmpleado> findPageTipoEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoEmpleadoLogic.findPageTipoEmpleado(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberTipoEmpleado() throws Exception {
        return tipoEmpleadoLogic.findTotalNumberTipoEmpleado();
    }

    public List<TipoEmpleadoDTO> getDataTipoEmpleado()
        throws Exception {
        return tipoEmpleadoLogic.getDataTipoEmpleado();
    }

    public void validateTipoEmpleado(TipoEmpleado tipoEmpleado)
        throws Exception {
        tipoEmpleadoLogic.validateTipoEmpleado(tipoEmpleado);
    }

    public List<TipoPago> getTipoPago() throws Exception {
        return tipoPagoLogic.getTipoPago();
    }

    public void saveTipoPago(TipoPago entity) throws Exception {
        tipoPagoLogic.saveTipoPago(entity);
    }

    public void deleteTipoPago(TipoPago entity) throws Exception {
        tipoPagoLogic.deleteTipoPago(entity);
    }

    public void updateTipoPago(TipoPago entity) throws Exception {
        tipoPagoLogic.updateTipoPago(entity);
    }

    public TipoPago getTipoPago(Long tipaId) throws Exception {
        TipoPago tipoPago = null;

        try {
            tipoPago = tipoPagoLogic.getTipoPago(tipaId);
        } catch (Exception e) {
            throw e;
        }

        return tipoPago;
    }

    public List<TipoPago> findByCriteriaInTipoPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return tipoPagoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoPago> findPageTipoPago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoPagoLogic.findPageTipoPago(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberTipoPago() throws Exception {
        return tipoPagoLogic.findTotalNumberTipoPago();
    }

    public List<TipoPagoDTO> getDataTipoPago() throws Exception {
        return tipoPagoLogic.getDataTipoPago();
    }

    public void validateTipoPago(TipoPago tipoPago) throws Exception {
        tipoPagoLogic.validateTipoPago(tipoPago);
    }

    /* ------------------------- NUEVOS METODOS ------------------------------*/
    
    
	@Override
	public Cliente BuscarClientePorCedula(Long cedula) throws Exception {	
		return clienteLogic.BuscarClientePorCedula(cedula);
	}
}
