package co.edu.usbcali.bodyfitness.presentation.businessDelegate;

import co.edu.usbcali.bodyfitness.modelo.Antropometria;
import co.edu.usbcali.bodyfitness.modelo.Cliente;
import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;
import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;
import co.edu.usbcali.bodyfitness.modelo.Empleado;
import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;
import co.edu.usbcali.bodyfitness.modelo.Rutina;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;
import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.modelo.control.AntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ClienteRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EjercicioRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.EmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteAntropometriaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IClienteRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEjercicioRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPlanClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IPlanLogic;
import co.edu.usbcali.bodyfitness.modelo.control.IRutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.ITipoPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PagoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PlanClienteLogic;
import co.edu.usbcali.bodyfitness.modelo.control.PlanLogic;
import co.edu.usbcali.bodyfitness.modelo.control.RutinaLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoEjercicioLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoEmpleadoLogic;
import co.edu.usbcali.bodyfitness.modelo.control.TipoPagoLogic;
import co.edu.usbcali.bodyfitness.modelo.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEjercicioDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoEmpleadoDTO;
import co.edu.usbcali.bodyfitness.modelo.dto.TipoPagoDTO;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IBusinessDelegatorView {
    public List<Antropometria> getAntropometria() throws Exception;

    public void saveAntropometria(Antropometria entity)
        throws Exception;

    public void deleteAntropometria(Antropometria entity)
        throws Exception;

    public void updateAntropometria(Antropometria entity)
        throws Exception;

    public Antropometria getAntropometria(Long antId) throws Exception;

    public List<Antropometria> findByCriteriaInAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<Antropometria> findPageAntropometria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberAntropometria() throws Exception;

    public List<AntropometriaDTO> getDataAntropometria()
        throws Exception;

    public void validateAntropometria(Antropometria antropometria)
        throws Exception;

    public List<Cliente> getCliente() throws Exception;

    public void saveCliente(Cliente entity) throws Exception;

    public void deleteCliente(Cliente entity) throws Exception;

    public void updateCliente(Cliente entity) throws Exception;

    public Cliente getCliente(Long cliId) throws Exception;

    public List<Cliente> findByCriteriaInCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Cliente> findPageCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberCliente() throws Exception;

    public List<ClienteDTO> getDataCliente() throws Exception;

    public void validateCliente(Cliente cliente) throws Exception;

    public List<ClienteAntropometria> getClienteAntropometria()
        throws Exception;

    public void saveClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    public void deleteClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    public void updateClienteAntropometria(ClienteAntropometria entity)
        throws Exception;

    public ClienteAntropometria getClienteAntropometria(Long cliantId)
        throws Exception;

    public List<ClienteAntropometria> findByCriteriaInClienteAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<ClienteAntropometria> findPageClienteAntropometria(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberClienteAntropometria() throws Exception;

    public List<ClienteAntropometriaDTO> getDataClienteAntropometria()
        throws Exception;

    public void validateClienteAntropometria(
        ClienteAntropometria clienteAntropometria) throws Exception;

    public List<ClienteRutina> getClienteRutina() throws Exception;

    public void saveClienteRutina(ClienteRutina entity)
        throws Exception;

    public void deleteClienteRutina(ClienteRutina entity)
        throws Exception;

    public void updateClienteRutina(ClienteRutina entity)
        throws Exception;

    public ClienteRutina getClienteRutina(Long clirutId)
        throws Exception;

    public List<ClienteRutina> findByCriteriaInClienteRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<ClienteRutina> findPageClienteRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberClienteRutina() throws Exception;

    public List<ClienteRutinaDTO> getDataClienteRutina()
        throws Exception;

    public void validateClienteRutina(ClienteRutina clienteRutina)
        throws Exception;

    public List<Ejercicio> getEjercicio() throws Exception;

    public void saveEjercicio(Ejercicio entity) throws Exception;

    public void deleteEjercicio(Ejercicio entity) throws Exception;

    public void updateEjercicio(Ejercicio entity) throws Exception;

    public Ejercicio getEjercicio(Long ejeId) throws Exception;

    public List<Ejercicio> findByCriteriaInEjercicio(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Ejercicio> findPageEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberEjercicio() throws Exception;

    public List<EjercicioDTO> getDataEjercicio() throws Exception;

    public void validateEjercicio(Ejercicio ejercicio)
        throws Exception;

    public List<EjercicioRutina> getEjercicioRutina() throws Exception;

    public void saveEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    public void deleteEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    public void updateEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    public EjercicioRutina getEjercicioRutina(Long ejerutId)
        throws Exception;

    public List<EjercicioRutina> findByCriteriaInEjercicioRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<EjercicioRutina> findPageEjercicioRutina(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberEjercicioRutina() throws Exception;

    public List<EjercicioRutinaDTO> getDataEjercicioRutina()
        throws Exception;

    public void validateEjercicioRutina(EjercicioRutina ejercicioRutina)
        throws Exception;

    public List<Empleado> getEmpleado() throws Exception;

    public void saveEmpleado(Empleado entity) throws Exception;

    public void deleteEmpleado(Empleado entity) throws Exception;

    public void updateEmpleado(Empleado entity) throws Exception;

    public Empleado getEmpleado(Long empId) throws Exception;

    public List<Empleado> findByCriteriaInEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Empleado> findPageEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberEmpleado() throws Exception;

    public List<EmpleadoDTO> getDataEmpleado() throws Exception;

    public void validateEmpleado(Empleado empleado) throws Exception;

    public List<Pago> getPago() throws Exception;

    public void savePago(Pago entity) throws Exception;

    public void deletePago(Pago entity) throws Exception;

    public void updatePago(Pago entity) throws Exception;

    public Pago getPago(Long pagId) throws Exception;

    public List<Pago> findByCriteriaInPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Pago> findPagePago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPago() throws Exception;

    public List<PagoDTO> getDataPago() throws Exception;

    public void validatePago(Pago pago) throws Exception;

    public List<Plan> getPlan() throws Exception;

    public void savePlan(Plan entity) throws Exception;

    public void deletePlan(Plan entity) throws Exception;

    public void updatePlan(Plan entity) throws Exception;

    public Plan getPlan(Long plaId) throws Exception;

    public List<Plan> findByCriteriaInPlan(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Plan> findPagePlan(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPlan() throws Exception;

    public List<PlanDTO> getDataPlan() throws Exception;

    public void validatePlan(Plan plan) throws Exception;

    public List<PlanCliente> getPlanCliente() throws Exception;

    public void savePlanCliente(PlanCliente entity) throws Exception;

    public void deletePlanCliente(PlanCliente entity) throws Exception;

    public void updatePlanCliente(PlanCliente entity) throws Exception;

    public PlanCliente getPlanCliente(Long plancliId) throws Exception;

    public List<PlanCliente> findByCriteriaInPlanCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<PlanCliente> findPagePlanCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPlanCliente() throws Exception;

    public List<PlanClienteDTO> getDataPlanCliente() throws Exception;

    public void validatePlanCliente(PlanCliente planCliente)
        throws Exception;

    public List<Rutina> getRutina() throws Exception;

    public void saveRutina(Rutina entity) throws Exception;

    public void deleteRutina(Rutina entity) throws Exception;

    public void updateRutina(Rutina entity) throws Exception;

    public Rutina getRutina(Long rutId) throws Exception;

    public List<Rutina> findByCriteriaInRutina(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Rutina> findPageRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberRutina() throws Exception;

    public List<RutinaDTO> getDataRutina() throws Exception;

    public void validateRutina(Rutina rutina) throws Exception;

    public List<TipoEjercicio> getTipoEjercicio() throws Exception;

    public void saveTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    public void deleteTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    public void updateTipoEjercicio(TipoEjercicio entity)
        throws Exception;

    public TipoEjercicio getTipoEjercicio(Long tiejeId)
        throws Exception;

    public List<TipoEjercicio> findByCriteriaInTipoEjercicio(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<TipoEjercicio> findPageTipoEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoEjercicio() throws Exception;

    public List<TipoEjercicioDTO> getDataTipoEjercicio()
        throws Exception;

    public void validateTipoEjercicio(TipoEjercicio tipoEjercicio)
        throws Exception;

    public List<TipoEmpleado> getTipoEmpleado() throws Exception;

    public void saveTipoEmpleado(TipoEmpleado entity) throws Exception;

    public void deleteTipoEmpleado(TipoEmpleado entity)
        throws Exception;

    public void updateTipoEmpleado(TipoEmpleado entity)
        throws Exception;

    public TipoEmpleado getTipoEmpleado(Long tiemId) throws Exception;

    public List<TipoEmpleado> findByCriteriaInTipoEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoEmpleado> findPageTipoEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoEmpleado() throws Exception;

    public List<TipoEmpleadoDTO> getDataTipoEmpleado()
        throws Exception;

    public void validateTipoEmpleado(TipoEmpleado tipoEmpleado)
        throws Exception;

    public List<TipoPago> getTipoPago() throws Exception;

    public void saveTipoPago(TipoPago entity) throws Exception;

    public void deleteTipoPago(TipoPago entity) throws Exception;

    public void updateTipoPago(TipoPago entity) throws Exception;

    public TipoPago getTipoPago(Long tipaId) throws Exception;

    public List<TipoPago> findByCriteriaInTipoPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoPago> findPageTipoPago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoPago() throws Exception;

    public List<TipoPagoDTO> getDataTipoPago() throws Exception;

    public void validateTipoPago(TipoPago tipoPago) throws Exception;
    
    /* ----------------------- NUEVOS METODOS --------------------------*/
    
    public Cliente BuscarClientePorCedula(Long cedula) throws Exception;
}
