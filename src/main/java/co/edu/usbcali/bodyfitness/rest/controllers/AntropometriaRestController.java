package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IAntropometriaMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/antropometria")
public class AntropometriaRestController {
    private static final Logger log = LoggerFactory.getLogger(AntropometriaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IAntropometriaMapper antropometriaMapper;

    @PostMapping(value = "/saveAntropometria")
    public void saveAntropometria(@RequestBody
    AntropometriaDTO antropometriaDTO) throws Exception {
        try {
            Antropometria antropometria = antropometriaMapper.antropometriaDTOToAntropometria(antropometriaDTO);

            businessDelegatorView.saveAntropometria(antropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteAntropometria/{antId}")
    public void deleteAntropometria(@PathVariable("antId")
    Long antId) throws Exception {
        try {
            Antropometria antropometria = businessDelegatorView.getAntropometria(antId);

            businessDelegatorView.deleteAntropometria(antropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateAntropometria/")
    public void updateAntropometria(
        @RequestBody
    AntropometriaDTO antropometriaDTO) throws Exception {
        try {
            Antropometria antropometria = antropometriaMapper.antropometriaDTOToAntropometria(antropometriaDTO);

            businessDelegatorView.updateAntropometria(antropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataAntropometria")
    public List<AntropometriaDTO> getDataAntropometria()
        throws Exception {
        try {
            return businessDelegatorView.getDataAntropometria();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getAntropometria/{antId}")
    public AntropometriaDTO getAntropometria(@PathVariable("antId")
    Long antId) throws Exception {
        try {
            Antropometria antropometria = businessDelegatorView.getAntropometria(antId);

            return antropometriaMapper.antropometriaToAntropometriaDTO(antropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
