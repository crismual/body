package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IClienteRutinaMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/clienteRutina")
public class ClienteRutinaRestController {
    private static final Logger log = LoggerFactory.getLogger(ClienteRutinaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IClienteRutinaMapper clienteRutinaMapper;

    @PostMapping(value = "/saveClienteRutina")
    public void saveClienteRutina(@RequestBody
    ClienteRutinaDTO clienteRutinaDTO) throws Exception {
        try {
            ClienteRutina clienteRutina = clienteRutinaMapper.clienteRutinaDTOToClienteRutina(clienteRutinaDTO);

            businessDelegatorView.saveClienteRutina(clienteRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteClienteRutina/{clirutId}")
    public void deleteClienteRutina(@PathVariable("clirutId")
    Long clirutId) throws Exception {
        try {
            ClienteRutina clienteRutina = businessDelegatorView.getClienteRutina(clirutId);

            businessDelegatorView.deleteClienteRutina(clienteRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateClienteRutina/")
    public void updateClienteRutina(
        @RequestBody
    ClienteRutinaDTO clienteRutinaDTO) throws Exception {
        try {
            ClienteRutina clienteRutina = clienteRutinaMapper.clienteRutinaDTOToClienteRutina(clienteRutinaDTO);

            businessDelegatorView.updateClienteRutina(clienteRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataClienteRutina")
    public List<ClienteRutinaDTO> getDataClienteRutina()
        throws Exception {
        try {
            return businessDelegatorView.getDataClienteRutina();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getClienteRutina/{clirutId}")
    public ClienteRutinaDTO getClienteRutina(
        @PathVariable("clirutId")
    Long clirutId) throws Exception {
        try {
            ClienteRutina clienteRutina = businessDelegatorView.getClienteRutina(clirutId);

            return clienteRutinaMapper.clienteRutinaToClienteRutinaDTO(clienteRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
