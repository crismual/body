package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IEjercicioMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/ejercicio")
public class EjercicioRestController {
    private static final Logger log = LoggerFactory.getLogger(EjercicioRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IEjercicioMapper ejercicioMapper;

    @PostMapping(value = "/saveEjercicio")
    public void saveEjercicio(@RequestBody
    EjercicioDTO ejercicioDTO) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioMapper.ejercicioDTOToEjercicio(ejercicioDTO);

            businessDelegatorView.saveEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteEjercicio/{ejeId}")
    public void deleteEjercicio(@PathVariable("ejeId")
    Long ejeId) throws Exception {
        try {
            Ejercicio ejercicio = businessDelegatorView.getEjercicio(ejeId);

            businessDelegatorView.deleteEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateEjercicio/")
    public void updateEjercicio(@RequestBody
    EjercicioDTO ejercicioDTO) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioMapper.ejercicioDTOToEjercicio(ejercicioDTO);

            businessDelegatorView.updateEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataEjercicio")
    public List<EjercicioDTO> getDataEjercicio() throws Exception {
        try {
            return businessDelegatorView.getDataEjercicio();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getEjercicio/{ejeId}")
    public EjercicioDTO getEjercicio(@PathVariable("ejeId")
    Long ejeId) throws Exception {
        try {
            Ejercicio ejercicio = businessDelegatorView.getEjercicio(ejeId);

            return ejercicioMapper.ejercicioToEjercicioDTO(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
