package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IEmpleadoMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.EmpleadoDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/empleado")
public class EmpleadoRestController {
    private static final Logger log = LoggerFactory.getLogger(EmpleadoRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IEmpleadoMapper empleadoMapper;

    @PostMapping(value = "/saveEmpleado")
    public void saveEmpleado(@RequestBody
    EmpleadoDTO empleadoDTO) throws Exception {
        try {
            Empleado empleado = empleadoMapper.empleadoDTOToEmpleado(empleadoDTO);

            businessDelegatorView.saveEmpleado(empleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteEmpleado/{empId}")
    public void deleteEmpleado(@PathVariable("empId")
    Long empId) throws Exception {
        try {
            Empleado empleado = businessDelegatorView.getEmpleado(empId);

            businessDelegatorView.deleteEmpleado(empleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateEmpleado/")
    public void updateEmpleado(@RequestBody
    EmpleadoDTO empleadoDTO) throws Exception {
        try {
            Empleado empleado = empleadoMapper.empleadoDTOToEmpleado(empleadoDTO);

            businessDelegatorView.updateEmpleado(empleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataEmpleado")
    public List<EmpleadoDTO> getDataEmpleado() throws Exception {
        try {
            return businessDelegatorView.getDataEmpleado();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getEmpleado/{empId}")
    public EmpleadoDTO getEmpleado(@PathVariable("empId")
    Long empId) throws Exception {
        try {
            Empleado empleado = businessDelegatorView.getEmpleado(empId);

            return empleadoMapper.empleadoToEmpleadoDTO(empleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
