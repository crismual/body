package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IPagoMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/pago")
public class PagoRestController {
    private static final Logger log = LoggerFactory.getLogger(PagoRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IPagoMapper pagoMapper;

    @PostMapping(value = "/savePago")
    public void savePago(@RequestBody
    PagoDTO pagoDTO) throws Exception {
        try {
            Pago pago = pagoMapper.pagoDTOToPago(pagoDTO);

            businessDelegatorView.savePago(pago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deletePago/{pagId}")
    public void deletePago(@PathVariable("pagId")
    Long pagId) throws Exception {
        try {
            Pago pago = businessDelegatorView.getPago(pagId);

            businessDelegatorView.deletePago(pago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updatePago/")
    public void updatePago(@RequestBody
    PagoDTO pagoDTO) throws Exception {
        try {
            Pago pago = pagoMapper.pagoDTOToPago(pagoDTO);

            businessDelegatorView.updatePago(pago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataPago")
    public List<PagoDTO> getDataPago() throws Exception {
        try {
            return businessDelegatorView.getDataPago();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getPago/{pagId}")
    public PagoDTO getPago(@PathVariable("pagId")
    Long pagId) throws Exception {
        try {
            Pago pago = businessDelegatorView.getPago(pagId);

            return pagoMapper.pagoToPagoDTO(pago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
