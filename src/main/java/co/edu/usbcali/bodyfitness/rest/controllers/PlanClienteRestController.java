package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IPlanClienteMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/planCliente")
public class PlanClienteRestController {
    private static final Logger log = LoggerFactory.getLogger(PlanClienteRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IPlanClienteMapper planClienteMapper;

    @PostMapping(value = "/savePlanCliente")
    public void savePlanCliente(@RequestBody
    PlanClienteDTO planClienteDTO) throws Exception {
        try {
            PlanCliente planCliente = planClienteMapper.planClienteDTOToPlanCliente(planClienteDTO);

            businessDelegatorView.savePlanCliente(planCliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deletePlanCliente/{plancliId}")
    public void deletePlanCliente(@PathVariable("plancliId")
    Long plancliId) throws Exception {
        try {
            PlanCliente planCliente = businessDelegatorView.getPlanCliente(plancliId);

            businessDelegatorView.deletePlanCliente(planCliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updatePlanCliente/")
    public void updatePlanCliente(@RequestBody
    PlanClienteDTO planClienteDTO) throws Exception {
        try {
            PlanCliente planCliente = planClienteMapper.planClienteDTOToPlanCliente(planClienteDTO);

            businessDelegatorView.updatePlanCliente(planCliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataPlanCliente")
    public List<PlanClienteDTO> getDataPlanCliente() throws Exception {
        try {
            return businessDelegatorView.getDataPlanCliente();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getPlanCliente/{plancliId}")
    public PlanClienteDTO getPlanCliente(
        @PathVariable("plancliId")
    Long plancliId) throws Exception {
        try {
            PlanCliente planCliente = businessDelegatorView.getPlanCliente(plancliId);

            return planClienteMapper.planClienteToPlanClienteDTO(planCliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
