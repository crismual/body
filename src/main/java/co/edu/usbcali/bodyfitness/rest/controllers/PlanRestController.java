package co.edu.usbcali.bodyfitness.rest.controllers;

import co.edu.usbcali.bodyfitness.dto.mapper.IPlanMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.modelo.dto.PlanDTO;
import co.edu.usbcali.bodyfitness.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/plan")
public class PlanRestController {
    private static final Logger log = LoggerFactory.getLogger(PlanRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IPlanMapper planMapper;

    @PostMapping(value = "/savePlan")
    public void savePlan(@RequestBody
    PlanDTO planDTO) throws Exception {
        try {
            Plan plan = planMapper.planDTOToPlan(planDTO);

            businessDelegatorView.savePlan(plan);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deletePlan/{plaId}")
    public void deletePlan(@PathVariable("plaId")
    Long plaId) throws Exception {
        try {
            Plan plan = businessDelegatorView.getPlan(plaId);

            businessDelegatorView.deletePlan(plan);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updatePlan/")
    public void updatePlan(@RequestBody
    PlanDTO planDTO) throws Exception {
        try {
            Plan plan = planMapper.planDTOToPlan(planDTO);

            businessDelegatorView.updatePlan(plan);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataPlan")
    public List<PlanDTO> getDataPlan() throws Exception {
        try {
            return businessDelegatorView.getDataPlan();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getPlan/{plaId}")
    public PlanDTO getPlan(@PathVariable("plaId")
    Long plaId) throws Exception {
        try {
            Plan plan = businessDelegatorView.getPlan(plaId);

            return planMapper.planToPlanDTO(plan);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
